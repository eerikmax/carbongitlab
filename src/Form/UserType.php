<?php

// src/Form/UserType.php
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validation;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', EmailType::class, [
              'label' => 'Email:', 'attr' => ['placeholder' => 'Use existing email']])
            ->add('plainPassword', RepeatedType::class, [
              'type' => PasswordType::class,
              'required' => true,
              'first_options'  => ['label' => 'Password:', 'attr' => ['placeholder' => 'Min. 8 characters']],
              'second_options' => ['label' => 'Password again:', 'attr' => ['placeholder' => 'Repeat the password']],
              'constraints' => [
                  new NotBlank(['message' => "This field is needed."]),
                  new Length([
                      'min' => 8,
                      'max' => 20,
                      'minMessage' => 'Minimum password length is {{ limit }} characters and max is 20.',
                  ]),
              ],
              'invalid_message' => 'Passwords do not match!',
              'options' => ['attr' => [
                  'class' => 'password-field',
                  'placeholder' => "Placeholder"
              ]],
          ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
