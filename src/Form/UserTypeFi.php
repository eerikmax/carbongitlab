<?php

// src/Form/UserType.php
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validation;

class UserTypeFi extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', EmailType::class, [
              'label' => 'Email:', 'attr' => ['placeholder' => 'Käytä olemassa olevaa sähköpostiosoitettasi']])
            ->add('plainPassword', RepeatedType::class, [
              'type' => PasswordType::class,
              'required' => true,
              'first_options'  => ['label' => 'Salasana:', 'attr' => ['placeholder' => 'Min. 8 merkkiä']],
              'second_options' => ['label' => 'Toista salasana:', 'attr' => ['placeholder' => 'Toista salasana']],
              'constraints' => [
                  new NotBlank(['message' => "Tämä kenttä on pakollinen."]),
                  new Length([
                      'min' => 8,
                      'max' => 20,
                      'minMessage' => 'Vähimmäispituus on {{ limit }} characters and maksimi 20 merkkiä.',
                  ]),
              ],
              'invalid_message' => 'Salasanat eivät täsmää!',
              'options' => ['attr' => [
                  'class' => 'password-field',
                  'placeholder' => "Placeholder"
              ]],
          ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
