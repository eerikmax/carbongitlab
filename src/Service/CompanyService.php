<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;


class CompanyService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function checkPermission($companyId, $userId)
    {

      $sql = "SELECT * FROM user_companies WHERE company_id = '".$companyId."' AND user_id = '".$userId."'";
      $stmt = $this->em->getConnection()->prepare($sql);
      $stmt->execute();
      $companyResults = $stmt->fetchAll();
      if ($companyResults) {
        return true;
      } else {
        return false;
      }

    }

    public function getCompany($company_id)
    {
        return $this->find($company_id);
    }

    public function getAllCompanies()
    {
        return $this->findAll();
    }

    public function addCompany($company)
    {
        return $this->save($company);
    }

    public function deleteCompany($id)
    {
        return $this->delete($this->find($id));
    }



}
