<?php

namespace App\Service;

use App\Entity\ArticleResult;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class ArticleResultService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getProperty($property_id)
    {
        return $this->find($property_id);
    }

    public function getAllArticleResults()
    {
        return $this->findAll();
    }

    public function getAllArticleResultsAsArray()
    {
      $query = $this->em->getRepository(ArticleResult::class)
      ->createQueryBuilder('c')
      ->getQuery();

      return $result = $query->getResult(Query::HYDRATE_ARRAY);

    }

    public function searchByOwner($owner) {

      $result = $this->em->getRepository(ArticleResult::class)
      ->createQueryBuilder('articleResult')
      ->select('articleResult, importedArticleAlias, propertyAlias')
      ->leftJoin('articleResult.importedArticle', 'importedArticleAlias')
      ->leftJoin('importedArticleAlias.property', 'propertyAlias')
      ->where('articleResult.owner LIKE :owner')
      ->setParameter('owner', '%'.$owner.'%')
      ->getQuery()
      ->getArrayResult();

      return $result;

    }

    public function searchByOwnerOLD($owner) {
      $result = $this->em->getRepository(ArticleResult::class)
      ->createQueryBuilder('u')
      ->where('u.owner LIKE :owner')
      ->setParameter('owner', '%'.$owner.'%')
      ->getQuery()
      ->getArrayResult();

      foreach ($result as $i => $product) {
        $sql = "SELECT importedArticle_id FROM article_result WHERE id = '".$result[$i]['id']."'";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $importedArticleIdResults = $stmt->fetch();
        $result[$i]['importedArticleId'] = $importedArticleIdResults['importedArticle_id'];
      }

      foreach ($result as $i => $product) {
        $sql = "SELECT * FROM imported_article WHERE id = '".$result[$i]['importedArticleId']."'";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $importedArticleResults = $stmt->fetch();
        $result[$i]['importedArticle'] = $importedArticleResults;
      }

      return $result;

    }

    public function addProperty($property)
    {
        return $this->save($property);
    }

    public function deleteProperty($id)
    {
        return $this->delete($this->find($id));
    }



}
