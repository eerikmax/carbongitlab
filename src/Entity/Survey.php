<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SurveyRepository")
 */
class Survey
{
  /**
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $name;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $surveyId;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $resultId;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $postId;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $lastUpdated;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $lastPublished;

  /**
  * @ORM\Column(type="text", length=90000)
  */
  private $surveyData;


   //Getters & Setters
  public function getId(){
    return $this->id;
  }

  public function getName(){
    return $this->name;
  }

  public function getSurveyId(){
    return $this->surveyId;
  }

  public function getResultId(){
    return $this->resultId;
  }

  public function getPostId(){
    return $this->postId;
  }

  public function getLastUpdated(){
    return $this->lastUpdated;
  }

  public function getLastPublished(){
    return $this->lastPublished;
  }

  public function getSurveyData(){
    return $this->surveyData;
  }

  public function setName($name){
    $this->name = $name;
  }

  public function setSurveyId($surveyId){
    $this->surveyId = $surveyId;
  }

  public function setResultId($resultId){
    $this->resultId = $resultId;
  }

  public function setPostId($postId){
    $this->postId = $postId;
  }

  public function setLastUpdated($lastUpdated){
    $this->lastUpdated = $lastUpdated;
  }

  public function setLastPublished($lastPublished){
    $this->lastPublished = $lastPublished;
  }

  public function setSurveyData($surveyData){
    $this->surveyData = $surveyData;
  }

}
