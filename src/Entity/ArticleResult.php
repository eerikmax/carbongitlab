<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleResultRepository")
 */
class ArticleResult
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $owner;

    /**
    * @Assert\Length(max=200)
   * @ORM\Column(type="integer")
   */
    private $date;

  /**
  * @Assert\Length(max=1000)
  * @ORM\Column(type="decimal", scale=2)
  */
  private $additionality5yearsHa;

  /**
  * @Assert\Length(max=1000)
  * @ORM\Column(type="decimal", scale=2)
  */
  private $additionality10yearsHa;

  /**
  * @Assert\Length(max=1000)
  * @ORM\Column(type="decimal", scale=2)
  */
  private $additionality5yearsPercentage;

  /**
  * @Assert\Length(max=1000)
  * @ORM\Column(type="decimal", scale=2)
  */
  private $additionality10yearsPercentage;

  /**
  * @ORM\ManyToOne(targetEntity="ImportedArticle", inversedBy="articleresults")
  * @ORM\JoinColumn(name="importedArticle_id", referencedColumnName="id")
  */
  private $importedArticle;

   //Getters & Setters
  public function getId(){
    return $this->id;
  }

  public function getOwner(){
    return $this->owner;
  }

 public function getDate(){
   return $this->date;
 }

  public function getPostOperationGrowth(){
    return $this->postOperationGrowth;
  }

  public function getCarbonSeqAdd(){
    return $this->carbonSeqAdd;
  }

  public function getPlanEffectTime(){
    return $this->planEffectTime;
  }

  public function getAdditionalityTotal(){
    return $this->additionalityTotal;
  }

/**
 * Get ImportedArticle
 *
 * @return \App\Entity\ImportedArticle
 */
  public function getImportedArticle()
  {
      return $this->importedArticle;
  }

  public function setOwner($owner){
    $this->owner = $owner;
  }

  public function setDate($date){
    $this->date = $date;
  }

  public function setPostOperationGrowth($postOperationGrowth){
    $this->postOperationGrowth = $postOperationGrowth;
  }

  public function setCarbonSeqAdd($carbonSeqAdd){
    $this->carbonSeqAdd = $carbonSeqAdd;
  }

  public function setPlanEffectTime($planEffectTime){
    $this->planEffectTime = $planEffectTime;
  }

  public function setAdditionalityTotal($additionalityTotal){
    $this->additionalityTotal = $additionalityTotal;
  }

  /**
 * Set importedArticle
 *
 * @param \App\Entity\ImportedArticle $importedArticle
 *
 * @return ImportedArticle
 */
  public function setImportedArticle(\App\Entity\ImportedArticle $importedArticle = null)
  {
      $this->importedArticle = $importedArticle;

      return $this;
  }

}
