<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnswerRepository")
 */
class Answer
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
  * @Assert\Length(max=200)
 * @ORM\Column(type="integer")
 */
  private $targetTime;

  /**
  * @ORM\Column(type="text", length=1000)
  */
  private $content;

  /**
  * @ORM\Column(type="text", length=1000)
  */
  private $formName;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $tabName;

  /**
  * @Assert\Length(max=200)
 * @ORM\Column(type="integer")
 */
  private $time;

  /**
  * @ORM\ManyToOne(targetEntity="Question", inversedBy="answers")
  * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
  */
  private $question;

  /**
  * @ORM\ManyToOne(targetEntity="User", inversedBy="answers")
  * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
  */
  private $user;

  /**
  * @ORM\ManyToOne(targetEntity="Company", inversedBy="answers")
  * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
  */
  private $company;

  /**
  * @ORM\ManyToOne(targetEntity="Factor", inversedBy="answers")
  * @ORM\JoinColumn(name="factor_id", referencedColumnName="id")
  */
  private $factor;


   //Getters & Setters
  public function getId() {
    return $this->id;
  }

  public function getTargetTime() {
    return $this->targetTime;
  }

  public function setTargetTime($targetTime){
    $this->targetTime = $targetTime;
  }

  public function getContent() {
    return $this->content;
  }

  public function setContent($content){
    $this->content = $content;
  }

  public function getFormName() {
    return $this->formName;
  }

  public function setFormName($formName){
    $this->formName = $formName;
  }

  public function getTabName() {
    return $this->tabName;
  }

  public function setTabName($tabName) {
    $this->tabName = $tabName;
  }

  public function getTime() {
    return $this->time;
  }

  public function setTime($time) {
    $this->time = $time;
  }

  /**
   * Get question
   *
   * @return \App\Entity\Question
   */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
   * Set question
   *
   * @param \App\Entity\Question $question
   *
   * @return Factor
   */
    public function setQuestion(\App\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
      public function getUser()
      {
          return $this->user;
      }

      /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return User
     */
      public function setUser(\App\Entity\User $user = null)
      {
          $this->user = $user;

          return $this;
      }

  /**
   * Get factor
   *
   * @return \App\Entity\Factor
   */
    public function getFactor()
    {
        return $this->factor;
    }

    /**
   * Set factor
   *
   * @param \App\Entity\Factor $factor
   *
   * @return Factor
   */
    public function setFactor(\App\Entity\Factor $factor = null)
    {
        $this->factor = $factor;

        return $this;
    }

    /**
     * Get company
     *
     * @return \App\Entity\Company
     */
      public function getCompany()
      {
          return $this->company;
      }

      /**
     * Set company
     *
     * @param \App\Entity\Company $company
     *
     * @return Company
     */
      public function setCompany(\App\Entity\Company $company = null)
      {
          $this->company = $company;

          return $this;
      }

}
