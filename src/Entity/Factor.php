<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FactorRepository")
 */
class Factor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $titleFi;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $titleEn;

  /**
  * @ORM\Column(type="decimal", scale=5)
 */
  private $value;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $measure;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $source;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $referenceId;

  /**
  * @Assert\Length(max=200)
 * @ORM\Column(type="integer")
 */
  private $lastUpdated;

  /**
  * @ORM\OneToMany(targetEntity="Question", mappedBy="factor")
  */
  private $questions;

  public function __construct()
  {
      $this->questions = new ArrayCollection();
  }

   //Getters & Setters
  public function getId(){
    return $this->id;
  }

  public function getTitleFi(){
    return $this->titleFi;
  }

  public function setTitleFi($titleFi){
    $this->titleFi = $titleFi;
  }

  public function getTitleEn(){
    return $this->titleEn;
  }

  public function setTitleEn($titleEn){
    $this->titleEn = $titleEn;
  }

  public function getValue(){
    return $this->value;
  }

  public function setValue($value){
    $this->value = $value;
  }

  public function getMeasure(){
    return $this->measure;
  }

  public function setMeasure($measure){
    $this->measure = $measure;
  }

  public function getSource(){
    return $this->source;
  }

  public function setSource($source){
    $this->source = $source;
  }

  public function getReferenceId(){
    return $this->referenceId;
  }

  public function setReferenceId($referenceId){
    $this->referenceId = $referenceId;
  }

  public function getLastUpdated(){
    return $this->lastUpdated;
  }

  public function setLastUpdated($lastUpdated){
    $this->lastUpdated = $lastUpdated;
  }

  /**
   * Get questions
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getQuestions()
  {
      return $this->questions;
  }

}
