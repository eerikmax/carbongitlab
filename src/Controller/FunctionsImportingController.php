<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\ImportedArticle;
use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Yectep\PhpSpreadsheetBundle\Factory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer as Writer;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;

use App\Service\ImportedArticleService as ServiceImportedArticle;
use App\Service\ArticleService as ArticleService;
use App\Service\PropertyService as PropertyService;

class FunctionsImportingController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  private function checkPermission($security) {
    if ($security->getUser() && $security->getUser()->getRole() == 'super-user') {
      return;
    } else {
      return $this->redirect('/');
    }
  }

  /**
 * @Route("/{locale}/functions/createfromforestkitxml", methods={"GET"})
 */
public function createPropertiesAndArticlesFromForestKitXml(Security $security, $locale, Request $request) {
  $response = $this->assertLocale($locale);
  if ($response) { return $response; }

  if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
    if ($security->getUser()->getRole() == 'super-user') {

      return $this->render('functions/'.$locale.'.uploadpropertiesfromforestkitxml.html.twig', array
      ('propertyId' => 1, 'propertyName' => 'propertyName', 'showXmlData' => false, 'xmlDataString' => '', 'invalidArticles' => false));

    } else {
      return $this->redirectToRoute('welcome');
    }
  } else {
    return $this->redirectToRoute('welcome');
  }

}

/**
 * @Route("/{locale}/functions/createfromforestkitxml", methods={"POST"})
 */
public function createMultiplePropertiesAndArticlesFromForestKitXmlPost(Security $security, $locale, Request $request) {
  $response = $this->assertLocale($locale);
  if ($response) { return $response; }

  if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

    $entityManager = $this->getDoctrine()->getManager();
    $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);

    if (isset($_FILES["xml"])) {

      if (isset($_FILES['xml']) && ($_FILES['xml']['error'] == UPLOAD_ERR_OK)) {

        //if not -> "wrong format!!!"
        $xml = simplexml_load_file($_FILES['xml']['tmp_name']);
        $str = $xml->asXML();

        $newStr = str_replace(array('ns4:', 'ns5:', 'ns7:', 'ns8:', 'ns9:', 'ns11:','ns15:','re:', 'st:', 'co:', 'gdt:', 'gml:', 'ts:', 'tst:', 'tss:'), '', $str);

        $articles = json_decode(json_encode((array) simplexml_load_string($newStr)), 1);

        $existingProperties = array();
        $existingPropertyNewArticles = array();
        $newProperties = array();
        $newPropertyArticles = array();

		if (isset($articles['Stands'])) {
			return new JsonResponse(array('message' => 'Stands outside estate (property) not allowed!'));
		}

        $properties = $articles['RealEstates']['RealEstate'];
        if (isset($properties['@attributes'])) {
          $properties = array($properties);
        }

		if (count($properties) > 1) {
			//return new JsonResponse(array('message' => 'Only 1 property in one file allowed!'));
		}

    //return new JsonResponse(array('message' => $properties));

		foreach ($properties as $i => $product) {
      $newParcels = array();
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels'][$k] as $m => $product) {
					//echo $properties[$i]['Parcels'][$k][$m]['ParcelNumber'];
					//echo 'test!';
					if (isset($properties[$i]['Parcels'][$k][$m]['Stands'])) {
						foreach ($properties[$i]['Parcels'][$k][$m]['Stands'] as $o => $product) {
							//echo count($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
							if (isset($properties[$i]['Parcels'][$k][$m]['Stands'][$o]['@attributes'])) {
								//echo 'is set!';
								$properties[$i]['Parcels'][$k][$m]['Stands'][$o] = array($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
							}
						}
					}
				}
			}

			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					if (isset($properties[$i]['Parcels']['Parcel']['@attributes'])) {
						$properties[$i]['Parcels']['Parcel'] = array($properties[$i]['Parcels']['Parcel']);
					}

					//foreach ($properties[$i]['Parcels'][$k]['Stands'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
					//}
				}
			}


			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					if (isset($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand']['@attributes'])) {
					//echo 'IS set!';
					$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] = array($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand']);
					}
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p] as $b => $product) {
								//$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['test'] = '123';
								if (isset($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
									$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
								}
							}
					}
				}
			}


		//return new JsonResponse(array('message' => $properties));

			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
						//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
						array_push($newParcels, $properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p]);
					}
				}
			}


		//return new JsonResponse(array('message' => $properties));

				foreach ($properties[$i]['Parcels'] as $k => $product) {
					if (!isset($properties[$i]['Parcels'][$k]['@attributes'])) {
						//echo 'IS not set!';
						$properties[$i]['Parcels'][$k]['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['Stand'] = $newParcels;
					}
					if (isset($properties[$i]['Parcels'][$k]['Stands'])) {
					foreach ($properties[$i]['Parcels'][$k]['Stands'] as $m => $product) {
						foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'] as $l => $product) {
							//echo 'test'.' ';
							foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l] as $b => $product) {
								//echo 'test'.' ';
								//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['@attributes']['id'];
								if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
									//echo 'is set!'.' ';
									$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']);
									foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1] as $h => $product) {
										//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
										if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
											$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
										}
									}
								}
							}
						}
					}
				}
			}


        //return new JsonResponse(array('message' => $properties));


          $properties[$i]['address'] = $properties[$i]['RealEstateName'];
          $properties[$i]['identifier'] = $properties[$i]['MunicipalityNumber'].'-'.
          $properties[$i]['AreaNumber'].'-'.$properties[$i]['GroupNumber'].'-'.$properties[$i]['UnitNumber'];

          $propertyNamesArray[] = ['address' => $properties[$i]['address'], 'identifier' => $properties[$i]['identifier']];
          $properties[$i]['owner'] = 'Testi Testerman';
          $properties[$i]['stands1'] = array();
		  if (isset($properties[$i]['Parcels'])) {
		  $parcels = $properties[$i]['Parcels'];
		  if (!isset($parcels['Parcel']['@attributes'])) {
			  //echo 'not isset!';
			  foreach ($parcels as $j => $product) {
				  foreach ($parcels[$j] as $m => $product) {
					  //array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand']);
					  foreach ($parcels[$j][$m]['Stands']['Stand'] as $p => $product) {
						  array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand'][$p]);
					  }
				  }

			  }
		  } else {
			  $properties[$i]['stands1'] = $properties[$i]['Parcels']['Parcel']['Stands']['Stand'];
		  }
		  } else {
			  $properties[$i]['stands1'] = $articles['Stands']['Stand'];
		  }
		  //return new JsonResponse(array('message' => $properties[$i]['stands1']));
		  //vrt 8 ja 9
		  //Notice: Undefined index: tTreeSpecies
		  //$properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
          unset($properties[$i]['@attributes']);
          unset($properties[$i]['RealEstateName']);
          unset($properties[$i]['MunicipalityNumber']);
          unset($properties[$i]['AreaNumber']);
          unset($properties[$i]['GroupNumber']);
          unset($properties[$i]['UnitNumber']);
          //unset($properties[$i]['Parcels']);

          foreach ($properties[$i]['stands1'] as $k => $product) {
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'])) {
				  $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'];
				}
            } else {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']) && isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
              $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'];
				} else {
					$properties[$i]['stands1'][$k]['totals'] = array();
				}
            }
			if (isset($properties[$i]['stands1'][$k]['StandBasicData'])) {
				$properties[$i]['stands1'][$k]['area'] = $properties[$i]['stands1'][$k]['StandBasicData']['Area'];
				$properties[$i]['stands1'][$k]['mainclass'] = $importedArticleService->getForestKitMainclass($properties[$i]['stands1'][$k]['StandBasicData']['MainGroup']);
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'])) {
					//echo $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'].' ';
					$properties[$i]['stands1'][$k]['standnumber'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'];
				}
				if (!is_array($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension']) && isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'])) {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'];
				} else {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
					$properties[$i]['stands1'][$k]['growplace'] = $importedArticleService->getForestKitGrowplace($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass']);
				} else {
				  $properties[$i]['stands1'][$k]['growplace'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SoilType'])) {
				$properties[$i]['stands1'][$k]['soiltype'] = $importedArticleService->getForestKitSoiltype($properties[$i]['stands1'][$k]['StandBasicData']['SoilType']);
				} else {
				  $properties[$i]['stands1'][$k]['soiltype'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass'])) {
				  $properties[$i]['stands1'][$k]['developmentclass'] = $importedArticleService->getForestKitDevelopmentclass($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass']);
				} else {
				  $properties[$i]['stands1'][$k]['developmentclass'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility'])) {
					$properties[$i]['stands1'][$k]['accessibility'] = $importedArticleService->getForestKitAccessibility($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility']);
				} else {
				  $properties[$i]['stands1'][$k]['accessibility'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'])) {
					$properties[$i]['stands1'][$k]['subgroup_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'];
				} else {
				  $properties[$i]['stands1'][$k]['subgroup_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
				$properties[$i]['stands1'][$k]['fertilityclass_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'];
				} else {
				  $properties[$i]['stands1'][$k]['fertilityclass_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality'])) {
					$properties[$i]['stands1'][$k]['quality'] = $importedArticleService->getForestKitQuality($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality']);
				} else {
				  $properties[$i]['stands1'][$k]['quality'] = '';
				}
				$properties[$i]['stands1'][$k]['operations'] = '';
			}
            $properties[$i]['stands1'][$k]['totals'] = array();
            //print_r($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
              $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanAge'];
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = ($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'] * $properties[$i]['stands1'][$k]['area']);
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'];
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['PulpWoodVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanDiameter'];
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanHeight'];
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['StemCount'];
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['BasalArea'];
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['VolumeGrowth'];
			} else {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = 0;
			}

			//['TreeStandDataDate'] <-- jos @attributes löytyy niin taulukkoa:
			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				$properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData'])) {
				$properties[$i]['stands1'][$k]['treeData'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
			}

            unset($properties[$i]['stands1'][$k]['StandBasicData']);

			if (isset($properties[$i]['stands1'][$k]['treeData'])) {
            foreach ($properties[$i]['stands1'][$k]['treeData'] as $j => $product) {
			  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies'])) {
				  $properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  $properties[$i]['stands1'][$k]['treeData'][$j]['age'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tAge'];
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = ($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'] * $properties[$i]['stands1'][$k]['area']);
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'];
				  } else {
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = 0;
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = 0;
				  }
				  $logVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'])) {
					$logVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['log_volume'] = $logVolume;
				  $pulpVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'])) {
					$pulpVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['pulp_volume'] = $pulpVolume;
				  $properties[$i]['stands1'][$k]['treeData'][$j]['diameter'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter'];
				  $properties[$i]['stands1'][$k]['treeData'][$j]['length'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight'];
				  $density = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'])) {
					$density = $properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['density'] = $density;

				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'];
				  } else {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = 0;
				  }

				  $growth = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'])) {
					$growth = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['growth'] = $growth;
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['@attributes']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['ChangeState']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStratumNumber']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStorey']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tAge']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogPercent']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['DataSource']);
			  }
            }
		  }
            unset($properties[$i]['stands1'][$k]['TreeStandData']); //totals
            unset($properties[$i]['stands1'][$k]['@attributes']);

            $sql = "SELECT * FROM property WHERE identifier = '".$properties[$i]['identifier']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $propertyResults = $stmt->fetch();

            if ($propertyResults) {
              //echo 'results!';
              $property = $entityManager->getRepository(Property::class)->find($propertyResults['id']);
              if (!$property) {
                return new JsonResponse(array('message' => 'No property found for identifier '.$propertyResults['id']));
              }
              $sql = "SELECT * FROM imported_article WHERE property_id = '".$propertyResults['id']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $propertyImportedArticleResults = $stmt->fetchAll();
              if ($propertyImportedArticleResults) {
                $propertyResults['importedArticles'] = $propertyImportedArticleResults;
              } else {
                $propertyResults['importedArticles'] = array();
              }
              $propertyData = array('propertyId' => $propertyResults['id'], 'identifier' => $propertyResults['identifier'], 'propertyName' => $propertyResults['address'], 'importedArticles' => $propertyResults['importedArticles']);
              if (!in_array($propertyData, $existingProperties)) {
                array_push($existingProperties, $propertyData);
              }

              $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);

              $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);
              $newImportedArticle->setProperty($property);
              $newArticle = array('propertyId' => $propertyResults['id'], 'importedArticle' => $newImportedArticle);
              array_push($existingPropertyNewArticles, $newArticle);


            } else {

              $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
              $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);

              $newArticle = array('propertyId' => $properties[$i]['identifier'], 'importedArticle' => $newImportedArticle);
              array_push($newPropertyArticles, $newArticle);
              array_push($newProperties, array('propertyId' => $properties[$i]['identifier'], 'propertyName' => $properties[$i]['address']));

            }

          }

      }

		$known = array();
		$filteredExistingProperties = array_filter($existingProperties, function ($val) use (&$known) {
			$unique = !in_array($val['propertyId'], $known);
		$known[] = $val['propertyId'];
		return $unique;
		});
		$filteredExistingProperties = array_values($filteredExistingProperties);

		foreach ($filteredExistingProperties as $i => $product) {
		  $filteredExistingProperties[$i]['newImportedArticles'] = array();
		  foreach ($existingPropertyNewArticles as $j => $product) {
			if ($filteredExistingProperties[$i]['propertyId'] == $existingPropertyNewArticles[$j]['propertyId']) {
			array_push($filteredExistingProperties[$i]['newImportedArticles'], $existingPropertyNewArticles[$j]['importedArticle']/*['importedArticles'][0]*/);
			}
		  }
		}

		$known = array();
		$filteredNewProperties = array_filter($newProperties, function ($val) use (&$known) {
			$unique = !in_array($val['propertyId'], $known);
		$known[] = $val['propertyId'];
		return $unique;
		});
		$filteredNewProperties = array_values($filteredNewProperties);

		$propertyName = 'propertyName';
		foreach ($filteredNewProperties as $i => $product) {
		  if (isset($filteredNewProperties[0])) {
			$propertyName = $filteredNewProperties[0]['propertyName'];
		  }
		  $filteredNewProperties[$i]['newImportedArticles'] = array();
		  foreach ($newPropertyArticles as $j => $product) {
			if ($filteredNewProperties[$i]['propertyId'] === $newPropertyArticles[$j]['propertyId']) {
			array_push($filteredNewProperties[$i]['newImportedArticles'], /*(array)*/$newPropertyArticles[$j]['importedArticle']/*['importedArticles'][0]*/);
			}
		  }
		}

		foreach ($filteredExistingProperties as $i => $product) {
		  foreach ($filteredExistingProperties[$i]['importedArticles'] as $k => $product) {
			$filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = false;
			if ($filteredExistingProperties[$i]['importedArticles'][$k]['manty_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['kuusi_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['rauduskoivu_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['hieskoivu_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['haapa_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['harmaaleppa_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['tervaleppa_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['lehtipuu_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['havupuu_age'] < 1) {
				  $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = true;
			}
		  }
		}

		foreach ($filteredExistingProperties as $i => $product) {
              foreach ($filteredExistingProperties[$i]['importedArticles'] as $k => $product) {
                $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = false;
                if ($filteredExistingProperties[$i]['importedArticles'][$k]['manty_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['kuusi_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['rauduskoivu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['hieskoivu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['haapa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['harmaaleppa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['tervaleppa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['lehtipuu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['havupuu_age'] < 1) {
                      $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = true;
                }
              }
            }

            foreach ($filteredExistingProperties as $i => $product) {
              foreach ($filteredExistingProperties[$i]['newImportedArticles'] as $k => $product) {
                $filteredExistingProperties[$i]['newImportedArticles'][$k]->noTreeData = false;
                if ($filteredExistingProperties[$i]['newImportedArticles'][$k]->getMantyAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getKuusiAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getRauduskoivuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHieskoivuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHaapaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHarmaaleppaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getTervaleppaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getLehtipuuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHavupuuAge() < 1) {
                      $filteredExistingProperties[$i]['newImportedArticles'][$k]->noTreeData = true;
                }
              }
            }

			$invalidArticles = false;
			$invalidArticlesArray = array();
			$invalidArtilesIndex = 0;
            foreach ($filteredNewProperties as $i => $product) {
              foreach ($filteredNewProperties[$i]['newImportedArticles'] as $k => $product) {
                $filteredNewProperties[$i]['newImportedArticles'][$k]->noTreeData = false;
                if ($filteredNewProperties[$i]['newImportedArticles'][$k]->getMantyAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getKuusiAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getRauduskoivuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHieskoivuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHaapaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHarmaaleppaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getTervaleppaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getLehtipuuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHavupuuAge() < 1) {
                      $filteredNewProperties[$i]['newImportedArticles'][$k]->noTreeData = true;
                }
				if ($filteredNewProperties[$i]['newImportedArticles'][$k]->getGrowplace() == '' ||
				$filteredNewProperties[$i]['newImportedArticles'][$k]->getDevelopmentclass() == '') {
					$invalidArticles = true;
					$invalidArtilesIndex++;
					array_push($invalidArticlesArray, array('index' => $invalidArtilesIndex, 'standId' => $filteredNewProperties[$i]['newImportedArticles'][$k]->getStandNumber()));
				}
              }
            }

        //return new JsonResponse(array('message' => $invalidArticlesArray));

        return $this->render('functions/'.$locale.'.uploadpropertiesfromforestkitxml.html.twig', array
        ('propertyId' => 1,
    		'propertyName' => $propertyName,
        'propertyNamesArray' => $propertyNamesArray,
        'existingProperties' => $filteredExistingProperties,
        'newProperties' => $filteredNewProperties,
        'existingPropertyArticles' => $existingPropertyNewArticles,
        'newPropertyArticles' => $newPropertyArticles,
        'showXmlData' => true,
    		'invalidArticles' => $invalidArticles,
    		'invalidArticlesArray' => $invalidArticlesArray,
        'xmlDataString' => $str ));

      }

    } else { //from string

      $allParams = $request->request->all();
      $addOnlyNew = isset($allParams['addOnlyNew']);

      $str = $allParams['xmlDataString'];
  	  $propertyName = $allParams['propertyName'];
      $propertyNamesArray = json_decode($allParams['propertyNamesArray'], true);
      //return new JsonResponse(array('propertyNamesArray' => $propertyNamesArray));

      $data = json_decode(json_encode((array) simplexml_load_string($str, 'SimpleXMLElement', LIBXML_NOWARNING)), 1);

      $newStr = str_replace(array('ns4:', 'ns5:', 'ns7:', 'ns8:', 'ns9:', 'ns11:','ns15:','re:', 'st:', 'co:', 'gdt:', 'gml:', 'ts:', 'tst:', 'tss:'), '', $str);

      $articles = json_decode(json_encode((array) simplexml_load_string($newStr)), 1);

	  //check ääkköset, new service method for the address
	  //return new JsonResponse(array('message' => $articles));

      $properties = $articles['RealEstates']['RealEstate'];
        if (isset($properties['@attributes'])) {
          $properties = array($properties);
       }

      if (count($properties) > 1) {
        //return new JsonResponse(array('message' => 'Only 1 property in one file allowed!'));
      }

      foreach ($properties as $i => $product) {
        $newParcels = array();
		foreach ($properties[$i]['Parcels'] as $k => $product) {
			foreach ($properties[$i]['Parcels'][$k] as $m => $product) {
				//echo $properties[$i]['Parcels'][$k][$m]['ParcelNumber'];
				//echo 'test!';
				if (isset($properties[$i]['Parcels'][$k][$m]['Stands'])) {
					foreach ($properties[$i]['Parcels'][$k][$m]['Stands'] as $o => $product) {
						//echo count($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
						if (isset($properties[$i]['Parcels'][$k][$m]['Stands'][$o]['@attributes'])) {
							//echo 'is set!';
							$properties[$i]['Parcels'][$k][$m]['Stands'][$o] = array($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
						}
					}
				}
			}
		}

			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					if (isset($properties[$i]['Parcels']['Parcel']['@attributes'])) {
						$properties[$i]['Parcels']['Parcel'] = array($properties[$i]['Parcels']['Parcel']);
					}

					//foreach ($properties[$i]['Parcels'][$k]['Stands'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
					//}
				}
			}

			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					if (isset($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand']['@attributes'])) {
					//echo 'IS set!';
					$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] = array($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand']);
					}
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p] as $b => $product) {
								//$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['test'] = '123';
								if (isset($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
									$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
								}
							}
					}
				}
			}

			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
						//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
						array_push($newParcels, $properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p]);
					}
				}
			}

		//return new JsonResponse(array('message' => $newParcels));

				foreach ($properties[$i]['Parcels'] as $k => $product) {
					if (!isset($properties[$i]['Parcels'][$k]['@attributes'])) {
						//echo 'IS not set!';
						$properties[$i]['Parcels'][$k]['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['Stand'] = $newParcels;
					}
					if (isset($properties[$i]['Parcels'][$k]['Stands'])) {
					foreach ($properties[$i]['Parcels'][$k]['Stands'] as $m => $product) {
						foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'] as $l => $product) {
							//echo 'test'.' ';
							foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l] as $b => $product) {
								//echo 'test'.' ';
								//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['@attributes']['id'];
								if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
									//echo 'is set!'.' ';
									$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']);
									foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1] as $h => $product) {
										//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
										if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
											$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
										}
									}
								}
							}
						}
					}
				}
			}

        //return new JsonResponse(array('message' => $properties));

          $properties[$i]['address'] = $properties[$i]['RealEstateName'];
          $properties[$i]['identifier'] = $properties[$i]['MunicipalityNumber'].'-'.
          $properties[$i]['AreaNumber'].'-'.$properties[$i]['GroupNumber'].'-'.$properties[$i]['UnitNumber'];

		  $sql = "SELECT id FROM property WHERE identifier = '".$properties[$i]['identifier']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $propertyResults = $stmt->fetch();
		  if ($propertyResults) {
			  $sql = "DELETE FROM imported_article WHERE property_id = '".$propertyResults['id']."'";
			  $stmt = $entityManager->getConnection()->prepare($sql);
			  $stmt->execute();
		  }

          $properties[$i]['owner'] = 'Testi Testerman';
          $properties[$i]['stands1'] = array();
		  if (isset($properties[$i]['Parcels'])) {
		  $parcels = $properties[$i]['Parcels'];
		  if (!isset($parcels['Parcel']['@attributes'])) {
			  //echo 'not isset!';
			  foreach ($parcels as $j => $product) {
				  foreach ($parcels[$j] as $m => $product) {
					  //array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand']);
					  foreach ($parcels[$j][$m]['Stands']['Stand'] as $p => $product) {
						  array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand'][$p]);
					  }
				  }

			  }
		  } else {
			  $properties[$i]['stands1'] = $properties[$i]['Parcels']['Parcel']['Stands']['Stand'];
		  }
		  } else {
			  $properties[$i]['stands1'] = $articles['Stands']['Stand'];
		  }
		  //return new JsonResponse(array('message' => $properties[$i]['stands1']));
		  //vrt 8 ja 9
		  //Notice: Undefined index: tTreeSpecies
		  //$properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
          unset($properties[$i]['@attributes']);
          unset($properties[$i]['RealEstateName']);
          unset($properties[$i]['MunicipalityNumber']);
          unset($properties[$i]['AreaNumber']);
          unset($properties[$i]['GroupNumber']);
          unset($properties[$i]['UnitNumber']);
          //unset($properties[$i]['Parcels']);


          foreach ($properties[$i]['stands1'] as $k => $product) {
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'])) {
				  $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'];
				}
            } else {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']) && isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
              $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'];
				} else {
					$properties[$i]['stands1'][$k]['totals'] = array();
				}
            }
			if (isset($properties[$i]['stands1'][$k]['StandBasicData'])) {
				$properties[$i]['stands1'][$k]['area'] = $properties[$i]['stands1'][$k]['StandBasicData']['Area'];
				$properties[$i]['stands1'][$k]['mainclass'] = $importedArticleService->getForestKitMainclass($properties[$i]['stands1'][$k]['StandBasicData']['MainGroup']);
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'])) {
					//echo $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'].' ';
					$properties[$i]['stands1'][$k]['standnumber'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'];
				}
				if (!is_array($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension']) && isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'])) {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'];
				} else {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
					$properties[$i]['stands1'][$k]['growplace'] = $importedArticleService->getForestKitGrowplace($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass']);
				} else {
				  $properties[$i]['stands1'][$k]['growplace'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SoilType'])) {
				$properties[$i]['stands1'][$k]['soiltype'] = $importedArticleService->getForestKitSoiltype($properties[$i]['stands1'][$k]['StandBasicData']['SoilType']);
				} else {
				  $properties[$i]['stands1'][$k]['soiltype'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass'])) {
				  $properties[$i]['stands1'][$k]['developmentclass'] = $importedArticleService->getForestKitDevelopmentclass($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass']);
				} else {
				  $properties[$i]['stands1'][$k]['developmentclass'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility'])) {
					$properties[$i]['stands1'][$k]['accessibility'] = $importedArticleService->getForestKitAccessibility($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility']);
				} else {
				  $properties[$i]['stands1'][$k]['accessibility'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'])) {
					$properties[$i]['stands1'][$k]['subgroup_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'];
				} else {
				  $properties[$i]['stands1'][$k]['subgroup_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
				$properties[$i]['stands1'][$k]['fertilityclass_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'];
				} else {
				  $properties[$i]['stands1'][$k]['fertilityclass_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality'])) {
					$properties[$i]['stands1'][$k]['quality'] = $importedArticleService->getForestKitQuality($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality']);
				} else {
				  $properties[$i]['stands1'][$k]['quality'] = '';
				}
				$properties[$i]['stands1'][$k]['operations'] = '';
			}
            $properties[$i]['stands1'][$k]['totals'] = array();
            //print_r($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
              $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanAge'];
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = ($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'] * $properties[$i]['stands1'][$k]['area']);
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'];
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['PulpWoodVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanDiameter'];
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanHeight'];
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['StemCount'];
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['BasalArea'];
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['VolumeGrowth'];
			} else {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = 0;
			}

			//['TreeStandDataDate'] <-- jos @attributes löytyy niin taulukkoa:
			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				$properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData'])) {
				$properties[$i]['stands1'][$k]['treeData'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
			}

            unset($properties[$i]['stands1'][$k]['StandBasicData']);

			if (isset($properties[$i]['stands1'][$k]['treeData'])) {
            foreach ($properties[$i]['stands1'][$k]['treeData'] as $j => $product) {
			  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies'])) {
				  $properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  $properties[$i]['stands1'][$k]['treeData'][$j]['age'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tAge'];
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = ($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'] * $properties[$i]['stands1'][$k]['area']);
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'];
				  } else {
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = 0;
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = 0;
				  }
				  $logVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'])) {
					$logVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['log_volume'] = $logVolume;
				  $pulpVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'])) {
					$pulpVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['pulp_volume'] = $pulpVolume;
				  $properties[$i]['stands1'][$k]['treeData'][$j]['diameter'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter'];
				  $properties[$i]['stands1'][$k]['treeData'][$j]['length'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight'];
				  $density = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'])) {
					$density = $properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['density'] = $density;

				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'];
				  } else {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = 0;
				  }

				  $growth = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'])) {
					$growth = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['growth'] = $growth;
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['@attributes']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['ChangeState']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStratumNumber']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStorey']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tAge']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogPercent']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['DataSource']);
			  }
            }
		  }
            unset($properties[$i]['stands1'][$k]['TreeStandData']); //totals
            unset($properties[$i]['stands1'][$k]['@attributes']);

			$propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);

            $sql = "SELECT * FROM property WHERE identifier = '".$properties[$i]['identifier']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $propertyResults = $stmt->fetch();

            if (!$propertyResults) {
                $property = new Property();
                $user = $security->getUser();
				//Tï¿½ysmï¿½ki
                $propertyName = $properties[$i]['address'];
                foreach ($propertyNamesArray as $l => $product) {
                  if ($propertyNamesArray[$l]['identifier'] == $properties[$i]['identifier']) {
                    $propertyName = $propertyNamesArray[$l]['address'];
                  }
                }

                $property->setAddress($propertyName);
                $property->setIdentifier($properties[$i]['identifier']);
                $property->setUser($security->getUser());
                $property->setOwner($allParams['owner']);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($property);
                $entityManager->flush();

                $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
                $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);
				$newImportedArticle->setProperty($property);

				if ($newImportedArticle->getGrowplace() !== '' && $newImportedArticle->getDevelopmentclass() !== '') {
					$entityManager->persist($newImportedArticle);
					$entityManager->flush();
				}
            } else {
                $property = $entityManager->getRepository(Property::class)->find($propertyResults['id']);
                if (!$property) {
                return new JsonResponse(array('message' => 'No property found for identifier '.$propertyResults['id']));
                }
                $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
                $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);
				$newImportedArticle->setProperty($property);

				if ($newImportedArticle->getGrowplace() !== '' && $newImportedArticle->getDevelopmentclass() !== '') {
					$entityManager->persist($newImportedArticle);
					$entityManager->flush();
				}

          }

          }

      }

	  //return new JsonResponse(array('message' => $properties));

      $this->addFlash('notice', 'Kiinteistöt ja kuviot lisättiin!');
      return $this->redirect('/'.$locale.'/property/');

    }

  } else {
    return $this->redirectToRoute('welcome');
  }

}

/**
 * @Route("/{locale}/functions/createfromforestkitxml", methods={"POST"})
 */
 //older function for adding 1 property per file
public function createPropertiesAndArticlesFromForestKitXmlPost(Security $security, $locale, Request $request) {
  $response = $this->assertLocale($locale);
  if ($response) { return $response; }

  if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

    $entityManager = $this->getDoctrine()->getManager();
    $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);

    if (isset($_FILES["xml"])) {

      if (isset($_FILES['xml']) && ($_FILES['xml']['error'] == UPLOAD_ERR_OK)) {

        //if not -> "wrong format!!!"
        $xml = simplexml_load_file($_FILES['xml']['tmp_name']);
        $str = $xml->asXML();

        $newStr = str_replace(array('ns4:', 'ns5:', 'ns7:', 'ns8:', 'ns9:', 'ns11:','ns15:','re:', 'st:', 'co:', 'gdt:', 'gml:', 'ts:', 'tst:', 'tss:'), '', $str);

        $articles = json_decode(json_encode((array) simplexml_load_string($newStr)), 1);

        $existingProperties = array();
        $existingPropertyNewArticles = array();
        $newProperties = array();
        $newPropertyArticles = array();

		if (isset($articles['Stands'])) {
			return new JsonResponse(array('message' => 'Stands outside estate (property) not allowed!'));
		}

        $properties = $articles['RealEstates']['RealEstate'];
        if (isset($properties['@attributes'])) {
          $properties = array($properties);
        }

		if (count($properties) > 1) {
			return new JsonResponse(array('message' => 'Only 1 property in one file allowed!'));
		}

		foreach ($properties as $i => $product) {
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels'][$k] as $m => $product) {
					//echo $properties[$i]['Parcels'][$k][$m]['ParcelNumber'];
					//echo 'test!';
					if (isset($properties[$i]['Parcels'][$k][$m]['Stands'])) {
						foreach ($properties[$i]['Parcels'][$k][$m]['Stands'] as $o => $product) {
							//echo count($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
							if (isset($properties[$i]['Parcels'][$k][$m]['Stands'][$o]['@attributes'])) {
								//echo 'is set!';
								$properties[$i]['Parcels'][$k][$m]['Stands'][$o] = array($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
							}
						}
					}
				}
			}
		}

		$newParcels = array();
		foreach ($properties as $i => $product) {
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					if (isset($properties[$i]['Parcels']['Parcel']['@attributes'])) {
						$properties[$i]['Parcels']['Parcel'] = array($properties[$i]['Parcels']['Parcel']);
					}

					//foreach ($properties[$i]['Parcels'][$k]['Stands'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
					//}
				}
			}
		}

		foreach ($properties as $i => $product) {
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p] as $b => $product) {
								//$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['test'] = '123';
								if (isset($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
									$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
								}
							}
					}
				}
			}
		}

		//return new JsonResponse(array('message' => $properties));

		foreach ($properties as $i => $product) {
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
						//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
						array_push($newParcels, $properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p]);
					}
				}
			}
		}

		//return new JsonResponse(array('message' => $properties));

		foreach ($properties as $i => $product) {
				foreach ($properties[$i]['Parcels'] as $k => $product) {
					if (!isset($properties[$i]['Parcels'][$k]['@attributes'])) {
						//echo 'IS not set!';
						$properties[$i]['Parcels'][$k]['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['Stand'] = $newParcels;
					}
					if (isset($properties[$i]['Parcels'][$k]['Stands'])) {
					foreach ($properties[$i]['Parcels'][$k]['Stands'] as $m => $product) {
						foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'] as $l => $product) {
							//echo 'test'.' ';
							foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l] as $b => $product) {
								//echo 'test'.' ';
								//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['@attributes']['id'];
								if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
									//echo 'is set!'.' ';
									$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']);
									foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1] as $h => $product) {
										//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
										if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
											$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
										}
									}
								}
							}
						}
					}
				}
			}
		}

        //return new JsonResponse(array('message' => $properties));

        foreach ($properties as $i => $product) {
          $properties[$i]['address'] = $properties[$i]['RealEstateName'];
          $properties[$i]['identifier'] = $properties[$i]['MunicipalityNumber'].'-'.
          $properties[$i]['AreaNumber'].'-'.$properties[$i]['GroupNumber'].'-'.$properties[$i]['UnitNumber'];
          $properties[$i]['owner'] = 'Testi Testerman';
          $properties[$i]['stands1'] = array();
		  if (isset($properties[$i]['Parcels'])) {
		  $parcels = $properties[$i]['Parcels'];
		  if (!isset($parcels['Parcel']['@attributes'])) {
			  //echo 'not isset!';
			  foreach ($parcels as $j => $product) {
				  foreach ($parcels[$j] as $m => $product) {
					  //array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand']);
					  foreach ($parcels[$j][$m]['Stands']['Stand'] as $p => $product) {
						  array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand'][$p]);
					  }
				  }

			  }
		  } else {
			  $properties[$i]['stands1'] = $properties[$i]['Parcels']['Parcel']['Stands']['Stand'];
		  }
		  } else {
			  $properties[$i]['stands1'] = $articles['Stands']['Stand'];
		  }
		  //return new JsonResponse(array('message' => $properties[$i]['stands1']));
		  //vrt 8 ja 9
		  //Notice: Undefined index: tTreeSpecies
		  //$properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
          unset($properties[$i]['@attributes']);
          unset($properties[$i]['RealEstateName']);
          unset($properties[$i]['MunicipalityNumber']);
          unset($properties[$i]['AreaNumber']);
          unset($properties[$i]['GroupNumber']);
          unset($properties[$i]['UnitNumber']);
          //unset($properties[$i]['Parcels']);

          foreach ($properties[$i]['stands1'] as $k => $product) {
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'])) {
				  $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'];
				}
            } else {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']) && isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
              $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'];
				} else {
					$properties[$i]['stands1'][$k]['totals'] = array();
				}
            }
			if (isset($properties[$i]['stands1'][$k]['StandBasicData'])) {
				$properties[$i]['stands1'][$k]['area'] = $properties[$i]['stands1'][$k]['StandBasicData']['Area'];
				$properties[$i]['stands1'][$k]['mainclass'] = $importedArticleService->getForestKitMainclass($properties[$i]['stands1'][$k]['StandBasicData']['MainGroup']);
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'])) {
					//echo $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'].' ';
					$properties[$i]['stands1'][$k]['standnumber'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'];
				}
				if (!is_array($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension']) && isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'])) {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'];
				} else {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
					$properties[$i]['stands1'][$k]['growplace'] = $importedArticleService->getForestKitGrowplace($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass']);
				} else {
				  $properties[$i]['stands1'][$k]['growplace'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SoilType'])) {
				$properties[$i]['stands1'][$k]['soiltype'] = $importedArticleService->getForestKitSoiltype($properties[$i]['stands1'][$k]['StandBasicData']['SoilType']);
				} else {
				  $properties[$i]['stands1'][$k]['soiltype'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass'])) {
				  $properties[$i]['stands1'][$k]['developmentclass'] = $importedArticleService->getForestKitDevelopmentclass($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass']);
				} else {
				  $properties[$i]['stands1'][$k]['developmentclass'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility'])) {
					$properties[$i]['stands1'][$k]['accessibility'] = $importedArticleService->getForestKitAccessibility($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility']);
				} else {
				  $properties[$i]['stands1'][$k]['accessibility'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'])) {
					$properties[$i]['stands1'][$k]['subgroup_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'];
				} else {
				  $properties[$i]['stands1'][$k]['subgroup_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
				$properties[$i]['stands1'][$k]['fertilityclass_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'];
				} else {
				  $properties[$i]['stands1'][$k]['fertilityclass_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality'])) {
					$properties[$i]['stands1'][$k]['quality'] = $importedArticleService->getForestKitQuality($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality']);
				} else {
				  $properties[$i]['stands1'][$k]['quality'] = '';
				}
				$properties[$i]['stands1'][$k]['operations'] = '';
			}
            $properties[$i]['stands1'][$k]['totals'] = array();
            //print_r($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
              $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanAge'];
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = ($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'] * $properties[$i]['stands1'][$k]['area']);
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'];
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['PulpWoodVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanDiameter'];
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanHeight'];
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['StemCount'];
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['BasalArea'];
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['VolumeGrowth'];
			} else {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = 0;
			}

			//['TreeStandDataDate'] <-- jos @attributes löytyy niin taulukkoa:
			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				$properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData'])) {
				$properties[$i]['stands1'][$k]['treeData'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
			}

            unset($properties[$i]['stands1'][$k]['StandBasicData']);

			if (isset($properties[$i]['stands1'][$k]['treeData'])) {
            foreach ($properties[$i]['stands1'][$k]['treeData'] as $j => $product) {
			  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies'])) {
				  $properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  $properties[$i]['stands1'][$k]['treeData'][$j]['age'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tAge'];
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = ($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'] * $properties[$i]['stands1'][$k]['area']);
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'];
				  } else {
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = 0;
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = 0;
				  }
				  $logVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'])) {
					$logVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['log_volume'] = $logVolume;
				  $pulpVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'])) {
					$pulpVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['pulp_volume'] = $pulpVolume;
				  $properties[$i]['stands1'][$k]['treeData'][$j]['diameter'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter'];
				  $properties[$i]['stands1'][$k]['treeData'][$j]['length'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight'];
				  $density = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'])) {
					$density = $properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['density'] = $density;

				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'];
				  } else {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = 0;
				  }

				  $growth = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'])) {
					$growth = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['growth'] = $growth;
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['@attributes']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['ChangeState']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStratumNumber']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStorey']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tAge']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogPercent']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['DataSource']);
			  }
            }
		  }
            unset($properties[$i]['stands1'][$k]['TreeStandData']); //totals
            unset($properties[$i]['stands1'][$k]['@attributes']);

            $sql = "SELECT * FROM property WHERE identifier = '".$properties[$i]['identifier']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $propertyResults = $stmt->fetch();

            if ($propertyResults) {
              //echo 'results!';
              $property = $entityManager->getRepository(Property::class)->find($propertyResults['id']);
              if (!$property) {
                return new JsonResponse(array('message' => 'No property found for identifier '.$propertyResults['id']));
              }
              $sql = "SELECT * FROM imported_article WHERE property_id = '".$propertyResults['id']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $propertyImportedArticleResults = $stmt->fetchAll();
              if ($propertyImportedArticleResults) {
                $propertyResults['importedArticles'] = $propertyImportedArticleResults;
              } else {
                $propertyResults['importedArticles'] = array();
              }
              $propertyData = array('propertyId' => $propertyResults['id'], 'identifier' => $propertyResults['identifier'], 'propertyName' => $propertyResults['address'], 'importedArticles' => $propertyResults['importedArticles']);
              if (!in_array($propertyData, $existingProperties)) {
                array_push($existingProperties, $propertyData);
              }

              $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);

              $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);
              $newImportedArticle->setProperty($property);
              $newArticle = array('propertyId' => $propertyResults['id'], 'importedArticle' => $newImportedArticle);
              array_push($existingPropertyNewArticles, $newArticle);


            } else {

              $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
              $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);

              $newArticle = array('propertyId' => $properties[$i]['identifier'], 'importedArticle' => $newImportedArticle);
              array_push($newPropertyArticles, $newArticle);
              array_push($newProperties, array('propertyId' => $properties[$i]['identifier'], 'propertyName' => $properties[$i]['address']));

            }

          }

      }

		$known = array();
		$filteredExistingProperties = array_filter($existingProperties, function ($val) use (&$known) {
			$unique = !in_array($val['propertyId'], $known);
		$known[] = $val['propertyId'];
		return $unique;
		});
		$filteredExistingProperties = array_values($filteredExistingProperties);

		foreach ($filteredExistingProperties as $i => $product) {
		  $filteredExistingProperties[$i]['newImportedArticles'] = array();
		  foreach ($existingPropertyNewArticles as $j => $product) {
			if ($filteredExistingProperties[$i]['propertyId'] == $existingPropertyNewArticles[$j]['propertyId']) {
			array_push($filteredExistingProperties[$i]['newImportedArticles'], $existingPropertyNewArticles[$j]['importedArticle']/*['importedArticles'][0]*/);
			}
		  }
		}

		$known = array();
		$filteredNewProperties = array_filter($newProperties, function ($val) use (&$known) {
			$unique = !in_array($val['propertyId'], $known);
		$known[] = $val['propertyId'];
		return $unique;
		});
		$filteredNewProperties = array_values($filteredNewProperties);

		$propertyName = 'propertyName';
		foreach ($filteredNewProperties as $i => $product) {
		  if (isset($filteredNewProperties[0])) {
			$propertyName = $filteredNewProperties[0]['propertyName'];
		  }
		  $filteredNewProperties[$i]['newImportedArticles'] = array();
		  foreach ($newPropertyArticles as $j => $product) {
			if ($filteredNewProperties[$i]['propertyId'] === $newPropertyArticles[$j]['propertyId']) {
			array_push($filteredNewProperties[$i]['newImportedArticles'], /*(array)*/$newPropertyArticles[$j]['importedArticle']/*['importedArticles'][0]*/);
			}
		  }
		}

		foreach ($filteredExistingProperties as $i => $product) {
		  foreach ($filteredExistingProperties[$i]['importedArticles'] as $k => $product) {
			$filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = false;
			if ($filteredExistingProperties[$i]['importedArticles'][$k]['manty_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['kuusi_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['rauduskoivu_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['hieskoivu_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['haapa_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['harmaaleppa_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['tervaleppa_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['lehtipuu_age'] < 1
				&& $filteredExistingProperties[$i]['importedArticles'][$k]['havupuu_age'] < 1) {
				  $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = true;
			}
		  }
		}

		foreach ($filteredExistingProperties as $i => $product) {
              foreach ($filteredExistingProperties[$i]['importedArticles'] as $k => $product) {
                $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = false;
                if ($filteredExistingProperties[$i]['importedArticles'][$k]['manty_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['kuusi_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['rauduskoivu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['hieskoivu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['haapa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['harmaaleppa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['tervaleppa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['lehtipuu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['havupuu_age'] < 1) {
                      $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = true;
                }
              }
            }

            foreach ($filteredExistingProperties as $i => $product) {
              foreach ($filteredExistingProperties[$i]['newImportedArticles'] as $k => $product) {
                $filteredExistingProperties[$i]['newImportedArticles'][$k]->noTreeData = false;
                if ($filteredExistingProperties[$i]['newImportedArticles'][$k]->getMantyAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getKuusiAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getRauduskoivuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHieskoivuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHaapaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHarmaaleppaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getTervaleppaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getLehtipuuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHavupuuAge() < 1) {
                      $filteredExistingProperties[$i]['newImportedArticles'][$k]->noTreeData = true;
                }
              }
            }

			$invalidArticles = false;
			$invalidArticlesArray = array();
			$invalidArtilesIndex = 0;
            foreach ($filteredNewProperties as $i => $product) {
              foreach ($filteredNewProperties[$i]['newImportedArticles'] as $k => $product) {
                $filteredNewProperties[$i]['newImportedArticles'][$k]->noTreeData = false;
                if ($filteredNewProperties[$i]['newImportedArticles'][$k]->getMantyAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getKuusiAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getRauduskoivuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHieskoivuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHaapaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHarmaaleppaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getTervaleppaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getLehtipuuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHavupuuAge() < 1) {
                      $filteredNewProperties[$i]['newImportedArticles'][$k]->noTreeData = true;
                }
				if ($filteredNewProperties[$i]['newImportedArticles'][$k]->getGrowplace() == '' ||
				$filteredNewProperties[$i]['newImportedArticles'][$k]->getDevelopmentclass() == '') {
					$invalidArticles = true;
					$invalidArtilesIndex++;
					array_push($invalidArticlesArray, array('index' => $invalidArtilesIndex, 'standId' => $filteredNewProperties[$i]['newImportedArticles'][$k]->getStandNumber()));
				}
              }
            }

        //return new JsonResponse(array('message' => $invalidArticlesArray));

        return $this->render('functions/'.$locale.'.uploadpropertiesfromforestkitxml.html.twig', array
        ('propertyId' => 1,
		'propertyName' => $propertyName,
        'existingProperties' => $filteredExistingProperties,
        'newProperties' => $filteredNewProperties,
        'existingPropertyArticles' => $existingPropertyNewArticles,
        'newPropertyArticles' => $newPropertyArticles,
        'showXmlData' => true,
		'invalidArticles' => $invalidArticles,
		'invalidArticlesArray' => $invalidArticlesArray,
        'xmlDataString' => $str ));

      }

    } else { //from string

      $allParams = $request->request->all();
      $addOnlyNew = isset($allParams['addOnlyNew']);

      $str = $allParams['xmlDataString'];
	  $propertyName = $allParams['propertyName'];

      $data = json_decode(json_encode((array) simplexml_load_string($str, 'SimpleXMLElement', LIBXML_NOWARNING)), 1);

      $newStr = str_replace(array('ns4:', 'ns5:', 'ns7:', 'ns8:', 'ns9:', 'ns11:','ns15:','re:', 'st:', 'co:', 'gdt:', 'gml:', 'ts:', 'tst:', 'tss:'), '', $str);

      $articles = json_decode(json_encode((array) simplexml_load_string($newStr)), 1);

	  //check ääkköset, new service method for the address
	  //return new JsonResponse(array('message' => $articles));

      $properties = $articles['RealEstates']['RealEstate'];
        if (isset($properties['@attributes'])) {
          $properties = array($properties);
       }

      if (count($properties) > 1) {
        return new JsonResponse(array('message' => 'Only 1 property in one file allowed!'));
      }

      foreach ($properties as $i => $product) {
		foreach ($properties[$i]['Parcels'] as $k => $product) {
			foreach ($properties[$i]['Parcels'][$k] as $m => $product) {
				//echo $properties[$i]['Parcels'][$k][$m]['ParcelNumber'];
				//echo 'test!';
				if (isset($properties[$i]['Parcels'][$k][$m]['Stands'])) {
					foreach ($properties[$i]['Parcels'][$k][$m]['Stands'] as $o => $product) {
						//echo count($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
						if (isset($properties[$i]['Parcels'][$k][$m]['Stands'][$o]['@attributes'])) {
							//echo 'is set!';
							$properties[$i]['Parcels'][$k][$m]['Stands'][$o] = array($properties[$i]['Parcels'][$k][$m]['Stands'][$o]);
						}
					}
				}
			}
		}
		}

		$newParcels = array();
		foreach ($properties as $i => $product) {
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					if (isset($properties[$i]['Parcels']['Parcel']['@attributes'])) {
						$properties[$i]['Parcels']['Parcel'] = array($properties[$i]['Parcels']['Parcel']);
					}

					//foreach ($properties[$i]['Parcels'][$k]['Stands'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
					//}
				}
			}
		}

		foreach ($properties as $i => $product) {
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p] as $b => $product) {
								//$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['test'] = '123';
								if (isset($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
									$properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p][$b]['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
								}
							}
					}
				}
			}
		}

		foreach ($properties as $i => $product) {
			foreach ($properties[$i]['Parcels'] as $k => $product) {
				foreach ($properties[$i]['Parcels']['Parcel'] as $l => $product) {
					//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
					foreach ($properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'] as $p => $product) {
						//array_push($newParcels, $properties[$i]['Parcels'][$k]);
						//echo $properties[$i]['Parcels']['Parcel'][$l]['@attributes']['id'];
						array_push($newParcels, $properties[$i]['Parcels']['Parcel'][$l]['Stands']['Stand'][$p]);
					}
				}
			}
		}

		//return new JsonResponse(array('message' => $newParcels));

		foreach ($properties as $i => $product) {
				foreach ($properties[$i]['Parcels'] as $k => $product) {
					if (!isset($properties[$i]['Parcels'][$k]['@attributes'])) {
						//echo 'IS not set!';
						$properties[$i]['Parcels'][$k]['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['@attributes'] = array();
						$properties[$i]['Parcels'][$k]['Stands']['Stand'] = $newParcels;
					}
					if (isset($properties[$i]['Parcels'][$k]['Stands'])) {
					foreach ($properties[$i]['Parcels'][$k]['Stands'] as $m => $product) {
						foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'] as $l => $product) {
							//echo 'test'.' ';
							foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l] as $b => $product) {
								//echo 'test'.' ';
								//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['@attributes']['id'];
								if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
									//echo 'is set!'.' ';
									$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate']);
									foreach ($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1] as $h => $product) {
										//echo $properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
										if (isset($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']['@attributes'])) {
											$properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'] = array($properties[$i]['Parcels'][$k]['Stands']['Stand'][$l]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum']);
										}
									}
								}
							}
						}
					}
				}
			}
		}

        //return new JsonResponse(array('message' => $properties));

        foreach ($properties as $i => $product) {
          $properties[$i]['address'] = $properties[$i]['RealEstateName'];
          $properties[$i]['identifier'] = $properties[$i]['MunicipalityNumber'].'-'.
          $properties[$i]['AreaNumber'].'-'.$properties[$i]['GroupNumber'].'-'.$properties[$i]['UnitNumber'];

		  $sql = "SELECT id FROM property WHERE identifier = '".$properties[$i]['identifier']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $propertyResults = $stmt->fetch();
		  if ($propertyResults) {
			  $sql = "DELETE FROM imported_article WHERE property_id = '".$propertyResults['id']."'";
			  $stmt = $entityManager->getConnection()->prepare($sql);
			  $stmt->execute();
		  }

          $properties[$i]['owner'] = 'Testi Testerman';
          $properties[$i]['stands1'] = array();
		  if (isset($properties[$i]['Parcels'])) {
		  $parcels = $properties[$i]['Parcels'];
		  if (!isset($parcels['Parcel']['@attributes'])) {
			  //echo 'not isset!';
			  foreach ($parcels as $j => $product) {
				  foreach ($parcels[$j] as $m => $product) {
					  //array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand']);
					  foreach ($parcels[$j][$m]['Stands']['Stand'] as $p => $product) {
						  array_push($properties[$i]['stands1'], $parcels[$j][$m]['Stands']['Stand'][$p]);
					  }
				  }

			  }
		  } else {
			  $properties[$i]['stands1'] = $properties[$i]['Parcels']['Parcel']['Stands']['Stand'];
		  }
		  } else {
			  $properties[$i]['stands1'] = $articles['Stands']['Stand'];
		  }
		  //return new JsonResponse(array('message' => $properties[$i]['stands1']));
		  //vrt 8 ja 9
		  //Notice: Undefined index: tTreeSpecies
		  //$properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
          unset($properties[$i]['@attributes']);
          unset($properties[$i]['RealEstateName']);
          unset($properties[$i]['MunicipalityNumber']);
          unset($properties[$i]['AreaNumber']);
          unset($properties[$i]['GroupNumber']);
          unset($properties[$i]['UnitNumber']);
          //unset($properties[$i]['Parcels']);


          foreach ($properties[$i]['stands1'] as $k => $product) {
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'])) {
				  $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['TreeStandSummary'];
				}
            } else {
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']) && isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
              $properties[$i]['stands1'][$k]['totals'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'];
				} else {
					$properties[$i]['stands1'][$k]['totals'] = array();
				}
            }
			if (isset($properties[$i]['stands1'][$k]['StandBasicData'])) {
				$properties[$i]['stands1'][$k]['area'] = $properties[$i]['stands1'][$k]['StandBasicData']['Area'];
				$properties[$i]['stands1'][$k]['mainclass'] = $importedArticleService->getForestKitMainclass($properties[$i]['stands1'][$k]['StandBasicData']['MainGroup']);
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'])) {
					//echo $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'].' ';
					$properties[$i]['stands1'][$k]['standnumber'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumber'];
				}
				if (!is_array($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension']) && isset($properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'])) {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = $properties[$i]['stands1'][$k]['StandBasicData']['StandNumberExtension'];
				} else {
					$properties[$i]['stands1'][$k]['standnumberExtension'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
					$properties[$i]['stands1'][$k]['growplace'] = $importedArticleService->getForestKitGrowplace($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass']);
				} else {
				  $properties[$i]['stands1'][$k]['growplace'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SoilType'])) {
				$properties[$i]['stands1'][$k]['soiltype'] = $importedArticleService->getForestKitSoiltype($properties[$i]['stands1'][$k]['StandBasicData']['SoilType']);
				} else {
				  $properties[$i]['stands1'][$k]['soiltype'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass'])) {
				  $properties[$i]['stands1'][$k]['developmentclass'] = $importedArticleService->getForestKitDevelopmentclass($properties[$i]['stands1'][$k]['StandBasicData']['DevelopmentClass']);
				} else {
				  $properties[$i]['stands1'][$k]['developmentclass'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility'])) {
					$properties[$i]['stands1'][$k]['accessibility'] = $importedArticleService->getForestKitAccessibility($properties[$i]['stands1'][$k]['StandBasicData']['Accessibility']);
				} else {
				  $properties[$i]['stands1'][$k]['accessibility'] = '';
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'])) {
					$properties[$i]['stands1'][$k]['subgroup_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['SubGroup'];
				} else {
				  $properties[$i]['stands1'][$k]['subgroup_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'])) {
				$properties[$i]['stands1'][$k]['fertilityclass_number'] = $properties[$i]['stands1'][$k]['StandBasicData']['FertilityClass'];
				} else {
				  $properties[$i]['stands1'][$k]['fertilityclass_number'] = 0;
				}
				if (isset($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality'])) {
					$properties[$i]['stands1'][$k]['quality'] = $importedArticleService->getForestKitQuality($properties[$i]['stands1'][$k]['StandBasicData']['StandQuality']);
				} else {
				  $properties[$i]['stands1'][$k]['quality'] = '';
				}
				$properties[$i]['stands1'][$k]['operations'] = '';
			}
            $properties[$i]['stands1'][$k]['totals'] = array();
            //print_r($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
              $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary'])) {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanAge'];
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = ($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'] * $properties[$i]['stands1'][$k]['area']);
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['Volume'];
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['SawLogVolume'])) {
					$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['PulpWoodVolume'];
				}
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanDiameter'];
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['MeanHeight'];
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['StemCount'];
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['BasalArea'];
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['TreeStandSummary']['VolumeGrowth'];
			} else {
				$properties[$i]['stands1'][$k]['totals']['totalAge'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalVolumeHa'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLogVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalPulpVolume'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDiameter'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalLength'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalDensity'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalBasalarea'] = 0;
				$properties[$i]['stands1'][$k]['totals']['totalGrowth'] = 0;
			}

			//['TreeStandDataDate'] <-- jos @attributes löytyy niin taulukkoa:
			if (isset($properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']['@attributes'])) {
				$properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'] = array(0 => 1, 1 => $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate']);
            }

			if (isset($properties[$i]['stands1'][$k]['TreeStandData'])) {
				$properties[$i]['stands1'][$k]['treeData'] = $properties[$i]['stands1'][$k]['TreeStandData']['TreeStandDataDate'][1]['tTreeStrata']['tTreeStratum'];
			}

            unset($properties[$i]['stands1'][$k]['StandBasicData']);

			if (isset($properties[$i]['stands1'][$k]['treeData'])) {
            foreach ($properties[$i]['stands1'][$k]['treeData'] as $j => $product) {
			  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies'])) {
				  $properties[$i]['stands1'][$k]['treeData'][$j]['treespecie'] = $importedArticleService->getForestKitTreespecie($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  $properties[$i]['stands1'][$k]['treeData'][$j]['age'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tAge'];
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = ($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'] * $properties[$i]['stands1'][$k]['area']);
					$properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolume'];
				  } else {
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume'] = 0;
					 $properties[$i]['stands1'][$k]['treeData'][$j]['volume_ha'] = 0;
				  }
				  $logVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'])) {
					$logVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['log_volume'] = $logVolume;
				  $pulpVolume = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'])) {
					$pulpVolume = $properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume'];
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['pulp_volume'] = $pulpVolume;
				  $properties[$i]['stands1'][$k]['treeData'][$j]['diameter'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter'];
				  $properties[$i]['stands1'][$k]['treeData'][$j]['length'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight'];
				  $density = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'])) {
					$density = $properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['density'] = $density;

				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'])) {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = $properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea'];
				  } else {
					$properties[$i]['stands1'][$k]['treeData'][$j]['basalarea'] = 0;
				  }

				  $growth = 0;
				  if (isset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'])) {
					$growth = $properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth'];
					unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  }
				  $properties[$i]['stands1'][$k]['treeData'][$j]['growth'] = $growth;
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['@attributes']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['ChangeState']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStratumNumber']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tTreeSpecies']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStorey']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tAge']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tBasalArea']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tStemCount']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanDiameter']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tMeanHeight']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogPercent']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tSawLogVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tPulpWoodVolume']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['tVolumeGrowth']);
				  unset($properties[$i]['stands1'][$k]['treeData'][$j]['DataSource']);
			  }
            }
		  }
            unset($properties[$i]['stands1'][$k]['TreeStandData']); //totals
            unset($properties[$i]['stands1'][$k]['@attributes']);

			$propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);

            $sql = "SELECT * FROM property WHERE identifier = '".$properties[$i]['identifier']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $propertyResults = $stmt->fetch();

            if (!$propertyResults) {
                $property = new Property();
                $user = $security->getUser();
				//Tï¿½ysmï¿½ki
                $property->setAddress($propertyName);
                $property->setIdentifier($properties[$i]['identifier']);
                $property->setUser($security->getUser());
                $property->setOwner($allParams['owner']);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($property);
                $entityManager->flush();

                $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
                $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);
				$newImportedArticle->setProperty($property);

				if ($newImportedArticle->getGrowplace() !== '' && $newImportedArticle->getDevelopmentclass() !== '') {
					$entityManager->persist($newImportedArticle);
					$entityManager->flush();
				}
            } else {
                $property = $entityManager->getRepository(Property::class)->find($propertyResults['id']);
                if (!$property) {
                return new JsonResponse(array('message' => 'No property found for identifier '.$propertyResults['id']));
                }
                $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
                $newImportedArticle = $importedArticleService->createImportedArticle($properties[$i]['stands1'][$k]);
				$newImportedArticle->setProperty($property);

				if ($newImportedArticle->getGrowplace() !== '' && $newImportedArticle->getDevelopmentclass() !== '') {
					$entityManager->persist($newImportedArticle);
					$entityManager->flush();
				}

          }

          }

      }

	  //return new JsonResponse(array('message' => $properties));

      $this->addFlash('notice', 'Kiinteistöt ja kuviot lisättiin!');
      return $this->redirect('/'.$locale.'/property/');

    }

  } else {
    return $this->redirectToRoute('welcome');
  }

}

    /**
   * @Route("/{locale}/functions/createfromxml", methods={"GET"})
   */
  public function createPropertiesAndArticlesFromXml(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
      if ($security->getUser()->getRole() == 'super-user') {

        return $this->render('functions/'.$locale.'.uploadpropertiesfromxml.html.twig', array
        ('propertyId' => 1, 'showXmlData' => false, 'xmlDataString' => ''));

      } else {
        return $this->redirectToRoute('welcome');
      }
    } else {
      return $this->redirectToRoute('welcome');
    }

  }

  /**
   * @Route("/{locale}/functions/createfromxml", methods={"POST"})
   */
  public function createPropertiesAndArticlesFromXmlPost(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

      $entityManager = $this->getDoctrine()->getManager();

      if (isset($_FILES["xml"])) {

        if (isset($_FILES['xml']) && ($_FILES['xml']['error'] == UPLOAD_ERR_OK)) {
            $entityManager = $this->getDoctrine()->getManager();
            $xml = simplexml_load_file($_FILES['xml']['tmp_name'], 'SimpleXMLElement', LIBXML_NOWARNING);
            $str = $xml->asXML();

            $data = json_decode(json_encode((array) simplexml_load_string($str, 'SimpleXMLElement', LIBXML_NOWARNING)), 1);

            if (count($data['table1']['Detail_Collection']['Detail']) <= 2) {
              $propertyParents = array($data['table1']['Detail_Collection']['Detail']);
            } else {
              $propertyParents = $data['table1']['Detail_Collection']['Detail'];
            }

            $existingProperties = array();
            $existingPropertyNewArticles = array();
            $newProperties = array();
            $newPropertyArticles = array();

            foreach ($propertyParents as $i => $product) {
              $itemDetails = $propertyParents[$i]['@attributes']['siteClass'];

              //remove properties and articles and test without pregmatching!!!!

              //return new JsonResponse(array('message' => $itemDetails));
              //to test if this XML is made by our system
              if (preg_match('/\&#xD;&#xA;\b/', $itemDetails)) {
              //    $itemDetails = str_replace("&#xD;&#xA;","\r\n", $itemDetails);
              }

              $detailPieces = explode("\n", $itemDetails);

              $propertyParents[$i]['area'] = $propertyParents[$i]['@attributes']['Area'];

              $propertyParents[$i]['property'] = str_replace("\r", '', $detailPieces[0]);
              $detailsArray = explode(" ", $propertyParents[$i]['property']);
              $propertyName = array_slice($detailsArray, 0, -1);
              $propertyNameString = implode(" ", $propertyName);
              $propertyParents[$i]['propertyName'] = $propertyNameString;
              $propertyParents[$i]['propertyId'] = end($detailsArray);

              $propertyParents[$i]['mainclass'] = str_replace("\r", '', $detailPieces[1]);
              $propertyParents[$i]['growplace'] = str_replace("\r", '', $detailPieces[2]);
              $propertyParents[$i]['soiltype'] = str_replace("\r", '', $detailPieces[3]);
              $propertyParents[$i]['developmentclass'] = str_replace("\r", '', $detailPieces[4]);
              $propertyParents[$i]['accessibility'] = str_replace("\r", '', $detailPieces[5]);
              if (isset($articleParents[$i]['accessibility']) && isset($detailPieces[6])) {
                $propertyParents[$i]['quality'] = str_replace("\r", '', $detailPieces[6]);
              } else {
                $propertyParents[$i]['quality'] = 'Ei määritelty';
              }

              //sql if exists already and needed to overwrite
              $sql = "SELECT * FROM property WHERE identifier = '".$propertyParents[$i]['propertyId']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $propertyResults = $stmt->fetch();

              if ($propertyResults) {
                $propertyParents[$i]['foundProperty'] = $propertyResults;
                $sql = "SELECT * FROM imported_article WHERE property_id = '".$propertyResults['id']."'";
                $stmt = $entityManager->getConnection()->prepare($sql);
                $stmt->execute();
                $propertyImportedArticleResults = $stmt->fetchAll();
                if ($propertyImportedArticleResults) {
                  $propertyParents[$i]['importedArticles'] = $propertyImportedArticleResults;
                } else {
                  $propertyParents[$i]['importedArticles'] = array();
                }
                if (!in_array($propertyParents[$i], $existingProperties)) {
                  array_push($existingProperties, array('propertyId' => $propertyParents[$i]['propertyId'], 'propertyName' => $propertyParents[$i]['propertyName'], 'importedArticles' => $propertyParents[$i]['importedArticles']));
                }
                //push array('propertyId'=>$propertyParents[$i]['propertyId'], 'articles'=>$propertyParents[$i]['importedArticles'])
                //return new JsonResponse(array('existingPropertyArticles' => $existingPropertyNewArticles));
                // this array('pro... inside foreach $propertyParents[$i]['importedArticles']: if not in array:
                $treeInfoHeader = array();
                if (isset($propertyParents[$i]['Subreport3']['Report']['table1'])) {
                  $treeInfoHeader = $propertyParents[$i]['Subreport3']['Report']['table1'];
                }

                $treeInfo = array();
                if (isset($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail'])) {
                  if (count($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail']) < 2) {
                    $treeInfo = array($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail']);
                  } else {
                    $treeInfo = $propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail'];
                  }
                }

                $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
                //here we don't need any property yet (not saving anything, just to display)
                $newImportedArticle = $importedArticleService->createImportedArticle($propertyParents[$i], $treeInfoHeader, $treeInfo);

                $newArticle = array('propertyId' => $propertyParents[$i]['propertyId'], 'importedArticle' => $newImportedArticle);
                array_push($existingPropertyNewArticles, $newArticle);

                //array_push($newProperties, array('propertyId' => $propertyParents[$i]['propertyId'], 'propertyName' => $propertyParents[$i]['propertyName'], 'importedArticles' => $propertyParents[$i]['importedArticles'])); //remove later
              } else {

                $treeInfoHeader = array();
                if (isset($propertyParents[$i]['Subreport3']['Report']['table1'])) {
                  $treeInfoHeader = $propertyParents[$i]['Subreport3']['Report']['table1'];
                }

                $treeInfo = array();
                if (isset($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail'])) {
                  if (count($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail']) < 2) {
                    $treeInfo = array($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail']);
                  } else {
                    $treeInfo = $propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail'];
                  }
                }

                $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
                //here we don't need any property yet (not saving anything, just to display)
                $newImportedArticle = $importedArticleService->createImportedArticle($propertyParents[$i], $treeInfoHeader, $treeInfo);

                $newArticle = array('propertyId' => $propertyParents[$i]['propertyId'], 'importedArticle' => $newImportedArticle);
                array_push($newPropertyArticles, $newArticle);
                array_push($newProperties, array('propertyId' => $propertyParents[$i]['propertyId'], 'propertyName' => $propertyParents[$i]['propertyName']));
                /*
                $propertyParents[$i]['importedArticles'] = array();
                if (!in_array($propertyParents[$i], $newProperties)) {
                  array_push($newProperties, array('propertyId' => $propertyParents[$i]['propertyId'], 'propertyName' => $propertyParents[$i]['propertyName'], 'importedArticles' => $propertyParents[$i]['importedArticles']));
                }
                array_push($newPropertyArticles, $propertyParents[$i]);*/
              }
            }

            $known = array();
            $filteredExistingProperties = array_filter($existingProperties, function ($val) use (&$known) {
                $unique = !in_array($val['propertyId'], $known);
            $known[] = $val['propertyId'];
            return $unique;
            });
            $filteredExistingProperties = array_values($filteredExistingProperties);

            foreach ($filteredExistingProperties as $i => $product) {
              $filteredExistingProperties[$i]['newImportedArticles'] = array();
              foreach ($existingPropertyNewArticles as $j => $product) {
                if ($filteredExistingProperties[$i]['propertyId'] == $existingPropertyNewArticles[$j]['propertyId']) {
                array_push($filteredExistingProperties[$i]['newImportedArticles'], $existingPropertyNewArticles[$j]['importedArticle']/*['importedArticles'][0]*/);
                }
              }
            }

            $known = array();
            $filteredNewProperties = array_filter($newProperties, function ($val) use (&$known) {
                $unique = !in_array($val['propertyId'], $known);
            $known[] = $val['propertyId'];
            return $unique;
            });
            $filteredNewProperties = array_values($filteredNewProperties);

            foreach ($filteredNewProperties as $i => $product) {
              $filteredNewProperties[$i]['newImportedArticles'] = array();
              foreach ($newPropertyArticles as $j => $product) {
                if ($filteredNewProperties[$i]['propertyId'] === $newPropertyArticles[$j]['propertyId']) {
                array_push($filteredNewProperties[$i]['newImportedArticles'], /*(array)*/$newPropertyArticles[$j]['importedArticle']/*['importedArticles'][0]*/);
                }
              }
            }

            foreach ($filteredExistingProperties as $i => $product) {
              foreach ($filteredExistingProperties[$i]['importedArticles'] as $k => $product) {
                $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = false;
                if ($filteredExistingProperties[$i]['importedArticles'][$k]['manty_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['kuusi_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['rauduskoivu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['hieskoivu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['haapa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['harmaaleppa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['tervaleppa_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['lehtipuu_age'] < 1
                    && $filteredExistingProperties[$i]['importedArticles'][$k]['havupuu_age'] < 1) {
                      $filteredExistingProperties[$i]['importedArticles'][$k]['noTreeData'] = true;
                }
              }
            }

            foreach ($filteredExistingProperties as $i => $product) {
              foreach ($filteredExistingProperties[$i]['newImportedArticles'] as $k => $product) {
                $filteredExistingProperties[$i]['newImportedArticles'][$k]->noTreeData = false;
                if ($filteredExistingProperties[$i]['newImportedArticles'][$k]->getMantyAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getKuusiAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getRauduskoivuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHieskoivuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHaapaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHarmaaleppaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getTervaleppaAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getLehtipuuAge() < 1
                    && $filteredExistingProperties[$i]['newImportedArticles'][$k]->getHavupuuAge() < 1) {
                      $filteredExistingProperties[$i]['newImportedArticles'][$k]->noTreeData = true;
                }
              }
            }

            foreach ($filteredNewProperties as $i => $product) {
              foreach ($filteredNewProperties[$i]['newImportedArticles'] as $k => $product) {
                $filteredNewProperties[$i]['newImportedArticles'][$k]->noTreeData = false;
                if ($filteredNewProperties[$i]['newImportedArticles'][$k]->getMantyAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getKuusiAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getRauduskoivuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHieskoivuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHaapaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHarmaaleppaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getTervaleppaAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getLehtipuuAge() < 1
                    && $filteredNewProperties[$i]['newImportedArticles'][$k]->getHavupuuAge() < 1) {
                      $filteredNewProperties[$i]['newImportedArticles'][$k]->noTreeData = true;
                }
              }
            }

            //return new JsonResponse(array('newProperties' => $filteredNewProperties));

            return $this->render('functions/'.$locale.'.uploadpropertiesfromxml.html.twig', array
            ('propertyId' => 1,
            'existingProperties' => $filteredExistingProperties,
            'newProperties' => $filteredNewProperties,
            'existingPropertyArticles' => $existingPropertyNewArticles,
            'newPropertyArticles' => $newPropertyArticles,
            'showXmlData' => true,
            'xmlDataString' => $str ));

        }

      } else { //from string

        $allParams = $request->request->all();
        $addOnlyNew = isset($allParams['addOnlyNew']);

        $str = $allParams['xmlDataString'];

        $data = json_decode(json_encode((array) simplexml_load_string($str, 'SimpleXMLElement', LIBXML_NOWARNING)), 1);

        if (count($data['table1']['Detail_Collection']['Detail']) <= 2) {
          $propertyParents = array($data['table1']['Detail_Collection']['Detail']);
        } else {
          $propertyParents = $data['table1']['Detail_Collection']['Detail'];
        }

        foreach ($propertyParents as $i => $product) {
          $itemDetails = $propertyParents[$i]['@attributes']['siteClass'];
          //return new JsonResponse(array('message' => $itemDetails));
          //to test if this XML is made by our system
          if (preg_match('/\&#xD;&#xA;\b/', $itemDetails)) {
          //    $itemDetails = str_replace("&#xD;&#xA;","\r\n", $itemDetails);
          }

          $detailPieces = explode("\n", $itemDetails);

          $propertyParents[$i]['area'] = $propertyParents[$i]['@attributes']['Area'];

          $propertyParents[$i]['property'] = str_replace("\r", '', $detailPieces[0]);
          $detailsArray = explode(" ", $propertyParents[$i]['property']);
          $propertyName = array_slice($detailsArray, 0, -1);
          $propertyNameString = implode(" ", $propertyName);
          $propertyParents[$i]['propertyName'] = $propertyNameString;
          $propertyParents[$i]['propertyId'] = end($detailsArray);


          $sql = "SELECT id FROM property WHERE identifier = '".$propertyParents[$i]['propertyId']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $propertyDbIdResults = $stmt->fetch();
          if ($propertyDbIdResults) {
            $propertyParents[$i]['propertyDbId'] = $propertyDbIdResults['id'];
            if (!$addOnlyNew) {
              $sql = "DELETE FROM imported_article WHERE property_id = '".$propertyParents[$i]['propertyDbId']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
            }
          } if (!$propertyDbIdResults) {
            $propertyParents[$i]['propertyDbId'] = null;
          }
        }

        if ($addOnlyNew) {
          //$filteredNewPropertyParents = array_filter($propertyParents, function (array $propertyParent): bool { return !$propertyParent['propertyDbId']; });
          $filteredNewPropertyParents = array();
          foreach ($propertyParents as $i => $product) {
            if (!$propertyParents[$i]['propertyDbId']) {
              array_push($filteredNewPropertyParents, $propertyParents[$i]);
            }
          }
          $propertyParents = $filteredNewPropertyParents;
        }

        foreach ($propertyParents as $i => $product) {
          $itemDetails = $propertyParents[$i]['@attributes']['siteClass'];
          //return new JsonResponse(array('message' => $itemDetails));
          //to test if this XML is made by our system

          if (preg_match('/\&#xD;&#xA;\b/', $itemDetails)) {
          //    $itemDetails = str_replace("&#xD;&#xA;","\r\n", $itemDetails);
          }

          $detailPieces = explode("\n", $itemDetails);

          $propertyParents[$i]['mainclass'] = str_replace("\r", '', $detailPieces[1]);
          $propertyParents[$i]['growplace'] = str_replace("\r", '', $detailPieces[2]);
          $propertyParents[$i]['soiltype'] = str_replace("\r", '', $detailPieces[3]);
          $propertyParents[$i]['developmentclass'] = str_replace("\r", '', $detailPieces[4]);
          $propertyParents[$i]['accessibility'] = str_replace("\r", '', $detailPieces[5]);
          if (isset($articleParents[$i]['accessibility']) && isset($detailPieces[6])) {
            $propertyParents[$i]['quality'] = str_replace("\r", '', $detailPieces[6]);
          } else {
            $propertyParents[$i]['quality'] = 'Ei määritelty';
          }

          $treeInfoHeader = array();
          if (isset($propertyParents[$i]['Subreport3']['Report']['table1'])) {
            $treeInfoHeader = $propertyParents[$i]['Subreport3']['Report']['table1'];
          }

          if (isset($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail'])) {
            if (count($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail']) < 2) {
              $propertyParents[$i]['treeinfo'] = array($propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail']);
            } else {
              $propertyParents[$i]['treeinfo'] = $propertyParents[$i]['Subreport3']['Report']['table1']['Detail_Collection']['Detail'];
            }
          } else {
            $propertyParents[$i]['treeinfo'] = array();
          }

          $sql = "SELECT * FROM property WHERE identifier = '".$propertyParents[$i]['propertyId']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $propertyResults = $stmt->fetch();
          if (!$propertyResults) {

            $property = new Property();
            $user = $security->getUser();
            $property->setAddress($propertyParents[$i]['propertyName']);
            $property->setIdentifier($propertyParents[$i]['propertyId']);
            $property->setUser($security->getUser());
            $property->setOwner($allParams['owner']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($property);
            $entityManager->flush();

            $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
            $newArticle = $importedArticleService->createImportedArticleWithFoundProperty($propertyParents[$i], $treeInfoHeader, $property);

            $entityManager->persist($newArticle);
            $entityManager->flush();
          } else {
              $property = $entityManager->getRepository(Property::class)->find($propertyResults['id']);
              if (!$property) {
                return new JsonResponse(array('message' => 'No property found for identifier '.$propertyResults['id']));
              }
              $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
              $newImportedArticle = $importedArticleService->createImportedArticleWithFoundProperty($propertyParents[$i], $treeInfoHeader, $property);

              $entityManager->persist($newImportedArticle);
              $entityManager->flush();

          }
        }

        $this->addFlash('notice', 'Kiinteistöt ja kuviot lisättiin!');
        return $this->redirect('/'.$locale.'/property/');

      }

    } else {
      return $this->redirectToRoute('welcome');
    }

  }


}
