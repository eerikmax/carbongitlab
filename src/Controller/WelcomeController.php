<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Security;

class WelcomeController extends Controller
{
    private function assertLocale($locale) {
      if ($locale !== 'fi' && $locale !== 'en') {
        return $this->redirect('/');
      }
    }
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Security $security)
    {
        //$this->addFlash('success', 'Welcome!');

        if( $this->isGranted('IS_AUTHENTICATED_FULLY') ){
          $username = $security->getUser()->getUsername();

          $locale = $security->getUser()->getLanguage();
          if ($locale) { $locale = 'en'; }

          return $this->render('index/fi.index.html.twig', ['username' => $username, 'error' => 'error']);
        } else {
          return $this->render('index/fi.index.html.twig', ['username' => 'username', 'error' => 'error']);
        }

    }

    /**
     * @Route("/en/", name="enIndex")
     */
    public function enIndexAction(Security $security)
    {
        //$this->addFlash('success', 'Welcome!');

        if( $this->isGranted('IS_AUTHENTICATED_FULLY') ){
          $username = $security->getUser()->getUsername();
          return $this->render('index/en.index.html.twig', ['username' => $username, 'error' => 'error']);
        } else {
          return $this->render('index/en.index.html.twig', ['username' => 'username', 'error' => 'error']);
        }

    }

    /**
     * @Route("/fi/", name="fiIndex")
     */
    public function fiIndexAction(Security $security)
    {
        //$this->addFlash('success', 'Welcome!');

        if( $this->isGranted('IS_AUTHENTICATED_FULLY') ){
          $username = $security->getUser()->getUsername();
          return $this->render('index/fi.index.html.twig', ['username' => $username, 'error' => 'error']);
        } else {
          return $this->render('index/fi.index.html.twig', ['username' => 'username', 'error' => 'error']);
        }

    }
    /**
     * @Route("/en/welcome", name="welcome")
     */
    public function welcomeAction()
    {
        //$response = $this->assertLocale($locale);
        //if ($response) { return $response; }

        return $this->render('welcome/en.index.html.twig', [
            'controller_name' => 'WelcomeController',
        ]);
    }

    /**
     * @Route("/fi/welcome", name="fiWelcome")
     */
    public function fiWelcomeAction()
    {
        //$response = $this->assertLocale($locale);
        //if ($response) { return $response; }

        return $this->render('welcome/fi.index.html.twig', [
            'controller_name' => 'WelcomeController',
        ]);
    }
}
