<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use App\Entity\Question;
use App\Entity\Factor;
use App\Entity\Answer;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
//services can have more functions
//service example: $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
//service example: $article = $articleService->getArticle($id);
use App\Service\AnswerService as ServiceAnswer;
use App\Service\UserService as ServiceUser;
use App\Service\PropertyService as ServiceProperty;
use App\Service\CompanyService as CompanyService;
use App\Service\QuestionService as QuestionService;

class UploadController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  /**
   * @Route("/{locale}/upload/{companyId}/", methods={"GET"})
   */
  public function upload(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

        $userId = $security->getUser()->getId();
        $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
        $hasPermission = $companyService->checkPermission($companyId, $userId);
        if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

        $results = array();
        // Paginate the results of the query
        $resultsPaginated = $paginator->paginate(
            // Doctrine Query, not results
            $results,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );

        return $this->render('upload/'.$locale.'.index.html.twig', array(
            'filteredBy' => true,
            'formName' => 'formName',
            'resultsPaginated' => $resultsPaginated));

      } else {
        return $this->redirectToRoute('welcome');
      }

  }

  /**
   * @Route("/{locale}/upload/{companyId}/", methods={"POST"})
   */
  public function uploadPost(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

        $file = $request->files->get('myfile');

        //$allParams = $request->request->all();
        //return new JsonResponse(array('message' => $allParams));

        if (empty($file)) {
            return new JsonResponse(array('message' => 'File '.$file->getClientOriginalName().' is empty!'));
        }

        $csv = array_map('str_getcsv', file($file));

        $result = array();
        array_walk_recursive($csv, function($v) use (&$result) {
            $result[] = $v;
        });

        return new JsonResponse(array('message' => $result));

      } else {
        return $this->redirectToRoute('welcome');
      }
  }
}
