<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\ImportedArticle;
use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use App\Service\ArticleService as ServiceArticle;
use App\Service\PropertyService as ServiceProperty;
use App\Service\ImportedArticleService as ServiceImportedArticle;

class PropertyController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  /**
   * @Route("/{locale}/property/{propertyId}/importedarticles/{articleId}/show", methods={"GET"})
   */
  public function showImportedArticle(Security $security, $locale, Request $request, $propertyId, $articleId) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $em = $this->getDoctrine()->getManager();
      $property = $em->getRepository(Property::class)->find($propertyId);

      $propertyUserId = $property->getUser()->getId();
      $userId = $security->getUser()->getId();

      if ($propertyUserId !== $userId) {
      //  return new JsonResponse(array('message' => 'No permission to property!'));
      }

      $importedArticle = $em->getRepository(ImportedArticle::class)->find($articleId);
      if (!$importedArticle) {
        return new JsonResponse(array('message' => 'No importedArticle!'));
      }

      $importedArticle->dateFi = date('m.Y', $importedArticle->getAdded());
      $importedArticle->dateEn = date('m/Y', $importedArticle->getAdded());
/*
      $articleResults = $importedArticle->getArticleResults();
      foreach ($articleResults as $i => $product) {
        $articleResults[$i]->dateFi = date('m.Y', $articleResults[$i]->getDate());
        $articleResults[$i]->dateEn = date('m/Y', $articleResults[$i]->getDate());
      }
      $importedArticle->articleResults = $articleResults;
*/
      return $this->render('properties/'.$locale.'.showimportedarticle.html.twig',array(
          'property' => $property, 'importedArticle' => $importedArticle));
  }

  /**
   * @Route("/{locale}/property/{propertyId}/importedarticles/{articleId}/show", methods={"POST"})
   */
  public function showImportedArticlePost(Security $security, $locale, Request $request, $propertyId, $articleId) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $em = $this->getDoctrine()->getManager();
      $property = $em->getRepository(Property::class)->find($propertyId);

      $propertyUserId = $property->getUser()->getId();
      $userId = $security->getUser()->getId();

      if ($propertyUserId !== $userId) {
      //  return new JsonResponse(array('message' => 'No permission to property!'));
      }

      $importedArticle = $em->getRepository(ImportedArticle::class)->find($articleId);
      if (!$importedArticle) {
        return new JsonResponse(array('message' => 'No importedArticle!'));
      }

      $allPostParams = $request->request->all();
      $allParams = $allPostParams['importedArticle'];

      $importedArticle->setArea($allParams['area']);
      $importedArticle->setMainclass($allParams['mainclass']);
      $importedArticle->setGrowplace($allParams['growplace']);
      $importedArticle->setSoiltype($allParams['soiltype']);
      $importedArticle->setDevelopmentclass($allParams['developmentclass']);
      $importedArticle->setAccessibility($allParams['accessibility']);
      $importedArticle->setQuality($allParams['quality']);
      $importedArticle->setOperations($allParams['operations']);
      $importedArticle->setAdded(time());
      $importedArticle->setProperty($property);

      $importedArticle->setTotalAge($allParams['totalAge']);
      $importedArticle->setTotalVolume($allParams['totalVolume']);
      $importedArticle->setTotalVolumeHa($allParams['totalVolumeHa']);
      $importedArticle->setTotalLogVolume($allParams['totalLogVolume']);
      $importedArticle->setTotalPulpVolume($allParams['totalPulpVolume']);
      $importedArticle->setTotalDiameter($allParams['totalDiameter']);
      $importedArticle->setTotalLength($allParams['totalLength']);
      $importedArticle->setTotalDensity($allParams['totalDensity']);
      $importedArticle->setTotalBasalarea($allParams['totalBasalarea']);
      $importedArticle->setTotalGrowth($allParams['totalGrowth']);

      $importedArticle->setMantyAge($allParams['mantyAge']);
      $importedArticle->setMantyVolume($allParams['mantyVolume']);
      $importedArticle->setMantyVolumeHa($allParams['mantyVolumeHa']);
      $importedArticle->setMantyLogVolume($allParams['mantyLogVolume']);
      $importedArticle->setMantyPulpVolume($allParams['mantyPulpVolume']);
      $importedArticle->setMantyDiameter($allParams['mantyDiameter']);
      $importedArticle->setMantyLength($allParams['mantyLength']);
      $importedArticle->setMantyDensity($allParams['mantyDensity']);
      $importedArticle->setMantyBasalarea($allParams['mantyBasalarea']);
      $importedArticle->setMantyGrowth($allParams['mantyGrowth']);

      $importedArticle->setKuusiAge($allParams['kuusiAge']);
      $importedArticle->setKuusiVolume($allParams['kuusiVolume']);
      $importedArticle->setKuusiVolumeHa($allParams['kuusiVolumeHa']);
      $importedArticle->setKuusiLogVolume($allParams['kuusiLogVolume']);
      $importedArticle->setKuusiPulpVolume($allParams['kuusiPulpVolume']);
      $importedArticle->setKuusiDiameter($allParams['kuusiDiameter']);
      $importedArticle->setKuusiLength($allParams['kuusiLength']);
      $importedArticle->setKuusiDensity($allParams['kuusiDensity']);
      $importedArticle->setKuusiBasalarea($allParams['kuusiBasalarea']);
      $importedArticle->setKuusiGrowth($allParams['kuusiGrowth']);

      $importedArticle->setRauduskoivuAge($allParams['rauduskoivuAge']);
      $importedArticle->setRauduskoivuVolume($allParams['rauduskoivuVolume']);
      $importedArticle->setRauduskoivuVolumeHa($allParams['rauduskoivuVolumeHa']);
      $importedArticle->setRauduskoivuLogVolume($allParams['rauduskoivuLogVolume']);
      $importedArticle->setRauduskoivuPulpVolume($allParams['rauduskoivuPulpVolume']);
      $importedArticle->setRauduskoivuDiameter($allParams['rauduskoivuDiameter']);
      $importedArticle->setRauduskoivuLength($allParams['rauduskoivuLength']);
      $importedArticle->setRauduskoivuDensity($allParams['rauduskoivuDensity']);
      $importedArticle->setRauduskoivuBasalarea($allParams['rauduskoivuBasalarea']);
      $importedArticle->setRauduskoivuGrowth($allParams['rauduskoivuGrowth']);

      $importedArticle->setHieskoivuAge($allParams['hieskoivuAge']);
      $importedArticle->setHieskoivuVolume($allParams['hieskoivuVolume']);
      $importedArticle->setHieskoivuVolumeHa($allParams['hieskoivuVolumeHa']);
      $importedArticle->setHieskoivuLogVolume($allParams['hieskoivuLogVolume']);
      $importedArticle->setHieskoivuPulpVolume($allParams['hieskoivuPulpVolume']);
      $importedArticle->setHieskoivuDiameter($allParams['hieskoivuDiameter']);
      $importedArticle->setHieskoivuLength($allParams['hieskoivuLength']);
      $importedArticle->setHieskoivuDensity($allParams['hieskoivuDensity']);
      $importedArticle->setHieskoivuBasalarea($allParams['hieskoivuBasalarea']);
      $importedArticle->setHieskoivuGrowth($allParams['hieskoivuGrowth']);

      $importedArticle->setHaapaAge($allParams['haapaAge']);
      $importedArticle->setHaapaVolume($allParams['haapaVolume']);
      $importedArticle->setHaapaVolumeHa($allParams['haapaVolumeHa']);
      $importedArticle->setHaapaLogVolume($allParams['haapaLogVolume']);
      $importedArticle->setHaapaPulpVolume($allParams['haapaPulpVolume']);
      $importedArticle->setHaapaDiameter($allParams['haapaDiameter']);
      $importedArticle->setHaapaLength($allParams['haapaLength']);
      $importedArticle->setHaapaDensity($allParams['haapaDensity']);
      $importedArticle->setHaapaBasalarea($allParams['haapaBasalarea']);
      $importedArticle->setHaapaGrowth($allParams['haapaGrowth']);

      $importedArticle->setHarmaaleppaAge($allParams['harmaaleppaAge']);
      $importedArticle->setHarmaaleppaVolume($allParams['harmaaleppaVolume']);
      $importedArticle->setHarmaaleppaVolumeHa($allParams['harmaaleppaVolumeHa']);
      $importedArticle->setHarmaaleppaLogVolume($allParams['harmaaleppaLogVolume']);
      $importedArticle->setHarmaaleppaPulpVolume($allParams['harmaaleppaPulpVolume']);
      $importedArticle->setHarmaaleppaDiameter($allParams['harmaaleppaDiameter']);
      $importedArticle->setHarmaaleppaLength($allParams['harmaaleppaLength']);
      $importedArticle->setHarmaaleppaDensity($allParams['harmaaleppaDensity']);
      $importedArticle->setHarmaaleppaBasalarea($allParams['harmaaleppaBasalarea']);
      $importedArticle->setHarmaaleppaGrowth($allParams['harmaaleppaGrowth']);

      $importedArticle->setTervaleppaAge($allParams['tervaleppaAge']);
      $importedArticle->setTervaleppaVolume($allParams['tervaleppaVolume']);
      $importedArticle->setTervaleppaVolumeHa($allParams['tervaleppaVolumeHa']);
      $importedArticle->setTervaleppaLogVolume($allParams['tervaleppaLogVolume']);
      $importedArticle->setTervaleppaPulpVolume($allParams['tervaleppaPulpVolume']);
      $importedArticle->setTervaleppaDiameter($allParams['tervaleppaDiameter']);
      $importedArticle->setTervaleppaLength($allParams['tervaleppaLength']);
      $importedArticle->setTervaleppaDensity($allParams['tervaleppaDensity']);
      $importedArticle->setTervaleppaBasalarea($allParams['tervaleppaBasalarea']);
      $importedArticle->setTervaleppaGrowth($allParams['tervaleppaGrowth']);

      $importedArticle->setHavupuuAge($allParams['havupuuAge']);
      $importedArticle->setHavupuuVolume($allParams['havupuuVolume']);
      $importedArticle->setHavupuuVolumeHa($allParams['havupuuVolumeHa']);
      $importedArticle->setHavupuuLogVolume($allParams['havupuuLogVolume']);
      $importedArticle->setHavupuuPulpVolume($allParams['havupuuPulpVolume']);
      $importedArticle->setHavupuuDiameter($allParams['havupuuDiameter']);
      $importedArticle->setHavupuuLength($allParams['havupuuLength']);
      $importedArticle->setHavupuuDensity($allParams['havupuuDensity']);
      $importedArticle->setHavupuuBasalarea($allParams['havupuuBasalarea']);
      $importedArticle->setHavupuuGrowth($allParams['havupuuGrowth']);

      $importedArticle->setLehtipuuAge($allParams['lehtipuuAge']);
      $importedArticle->setLehtipuuVolume($allParams['lehtipuuVolume']);
      $importedArticle->setLehtipuuVolumeHa($allParams['lehtipuuVolumeHa']);
      $importedArticle->setLehtipuuLogVolume($allParams['lehtipuuLogVolume']);
      $importedArticle->setLehtipuuPulpVolume($allParams['lehtipuuPulpVolume']);
      $importedArticle->setLehtipuuDiameter($allParams['lehtipuuDiameter']);
      $importedArticle->setLehtipuuLength($allParams['lehtipuuLength']);
      $importedArticle->setLehtipuuDensity($allParams['lehtipuuDensity']);
      $importedArticle->setLehtipuuBasalarea($allParams['lehtipuuBasalarea']);
      $importedArticle->setLehtipuuGrowth($allParams['lehtipuuGrowth']);

      $totalGrowth = $importedArticle->getMantyGrowth()+
      $importedArticle->getKuusiGrowth()+
      $importedArticle->getRauduskoivuGrowth()+
      $importedArticle->getHieskoivuGrowth()+
      $importedArticle->getHaapaGrowth()+
      $importedArticle->getHarmaaleppaGrowth()+
      $importedArticle->getTervaleppaGrowth()+
      $importedArticle->getLehtipuuGrowth()+
      $importedArticle->getHavupuuGrowth();

      $importedArticle->setTotalGrowth($totalGrowth);

      $em->persist($importedArticle);
      $em->flush();

      $this->addFlash('notice', 'Kuvion tiedot päivitettiin.');
      return $this->redirect('/'.$locale.'/property/'.$propertyId.'/importedarticles/'.$articleId.'/show');
  }

  /**
   * @Route ("/{locale}/property/{propertyId}/importedarticles/{articleId}/delete", methods={"GET"})
   */
  public function deleteImportedArticle(Security $security, $locale, Request $request, $propertyId, $articleId) {

    $response = $this->assertLocale($locale);
    if ($response) { return $response; }
	/*
    if ($security->getUser()->getRole() !== 'super-user') {
      return $this->redirectToRoute('welcome');
    }
	*/
    $em = $this->getDoctrine()->getManager();
    $property = $em->getRepository(Property::class)->find($propertyId);

    $importedArticle = $em->getRepository(ImportedArticle::class)->find($articleId);
    if (!$importedArticle) {
      return new JsonResponse(array('message' => 'No importedArticle!'));
    }

    $articleResults = $importedArticle->getArticleResults();
    foreach ($articleResults as $i => $product) {
      $sql = "DELETE FROM article_result WHERE id = '".$articleResults[$i]->getId()."'";
      $stmt = $em->getConnection()->prepare($sql);
      $stmt->execute();
    }

    $sql = "DELETE FROM imported_article WHERE id = '".$articleId."'";
    $stmt = $em->getConnection()->prepare($sql);
    $stmt->execute();

    $this->addFlash('notice', 'Kuvio poistettiin.');
    return $this->redirect('/'.$locale.'/property/'.$propertyId.'/');

  }

    /**
     * @Route("/{locale}/property", name="property_list", methods={"GET"})
     */
    public function property(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
          $userId = $security->getUser()->getId();
          //$user = $this->getDoctrine()->getRepository(User::class)->find($userId);
          //$properties = $user->getProperties();
          $em = $this->getDoctrine()->getManager();
          $propertyService  = new ServiceProperty($this->getDoctrine()->getManager(), Property::class);
		  
		  //return new JsonResponse(array('message' => $security->getUser()->getRole()));
		
          if ($security->getUser()->getRole() == 'super-user') {
            $properties = $propertyService->getAllProperties();
          } if ($security->getUser()->getRole() == 'customer-admin') {
            $properties = $propertyService->getAllProperties();
          } else {
            $properties = $em->getRepository(Property::class)
            ->createQueryBuilder('u')
            ->where('u.user = '.$userId)
            ->getQuery()
            ->getResult();
          }
		
          $properties = $propertyService->getAllProperties();
		
          $totalAreaSum = 0;
          $totalSizeSum = 0;
          $totalArticleQuantity = 0;
          foreach ($properties as $i => $product) {
             $properties[$i]->userId = $properties[$i]->getUser()->getId();

             $properties[$i]->username = $properties[$i]->getUser()->getUsername();

             foreach ($articles = $properties[$i]->getArticles() as $k => $product) {
                $totalAreaSum = $totalAreaSum + $articles[$k]->getArea();

                $totalArticleSizeSum = intval($articles[$k]->getMat()) +
                intval($articles[$k]->getKut()) +
                intval($articles[$k]->getKot()) +
                intval($articles[$k]->getMut()) +
                intval($articles[$k]->getMak()) +
                intval($articles[$k]->getKuk()) +
                intval($articles[$k]->getKok()) +
                intval($articles[$k]->getMuk()) +
                intval($articles[$k]->getEnp());
                $totalSizeSum = $totalSizeSum + $totalArticleSizeSum;

                $totalArticleQuantity = $totalArticleQuantity + 1;
             }
          }

          $propertiesSummary = array(
            'totalAreaSum' => $totalAreaSum,
            'totalSizeSum' => $totalSizeSum,
            'totalValue' => 982,
            'totalArticleQuantity' => $totalArticleQuantity
          );

          return $this->render('properties/'.$locale.'.index.html.twig', array
          ('propertiesSummary' => $propertiesSummary, 'properties' => $properties));
        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route ("/{locale}/newproperty", methods={"GET"})
     */
    public function new(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $newProperty = new Property();

        $allProperties  = new ServiceArticle($this->getDoctrine()->getManager(),Property::class);
        $username = $security->getUser()->getUsername();
        //$user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        return $this->render('properties/'.$locale.'.new.html.twig',array(
            'property'=>$newProperty, 'username'=>$username ));
    }

    /**
     * @Route ("/{locale}/newproperty", methods={"POST"})
     */
    public function newPost(Security $security, Request $request, $locale) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $property = new Property();

      $user = $security->getUser();

      $property->setAddress($request->get('address'));
      $property->setIdentifier($request->get('identifier'));
      $property->setUser($security->getUser());
      $property->setOwner($request->get('owner'));

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($property);
      $entityManager->flush();

      $propertyId = $property->getId();
      return $this->redirect('/'.$locale.'/property/'.$propertyId);
    }

    /**
     * @Route ("/{locale}/property/{id}", name= "property_show", methods={"GET"})
     */
    public function show(Security $security, Request $request, PaginatorInterface $paginator, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $property = $this->getDoctrine()->getRepository(Property::class)->find($id);

        if (!$property) {
          return $this->redirect('/');
        }

        $propertyUserId = $property->getUser()->getId();
        $userId = $security->getUser()->getId();
		/*
        if ($propertyUserId !== $userId) {
          throw $this->createNotFoundException(
            'No property found for this user!'
          );
        }
		*/
        $property->userId = $property->getUser()->getId();

        $property->username = $property->getUser()->getUsername();
        $articles = $property->getArticles();
        $importedArticles = $property->getImportedArticles();

        foreach ($importedArticles as $i => $product) {
          $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
          $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
        }

        $importedArticlesPaginated = $paginator->paginate(
            // Doctrine Query, not results
            $importedArticles,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5);

        foreach ($articles as $i => $product) {
           $articles[$i]->propertyId = $articles[$i]->getProperty()->getId();

           $totalSize = intval($articles[$i]->getMat()) +
           intval($articles[$i]->getKut()) +
           intval($articles[$i]->getKot()) +
           intval($articles[$i]->getMut()) +
           intval($articles[$i]->getMak()) +
           intval($articles[$i]->getKuk()) +
           intval($articles[$i]->getKok()) +
           intval($articles[$i]->getMuk()) +
           intval($articles[$i]->getEnp());

           $articleSummary = array(
             'totalSize'=> $totalSize,
             'propertyAddress'=> $articles[$i]->getProperty()->getAddress(),
             'areaValue' => 982
           );

           $articles[$i]->articleSummary = $articleSummary;
        }

        $propertySizeSummary = 0;
        $propertyAreaSummary = 0;
        $propertyAreaValueSummary = 0;
        foreach ($articles as $i => $product) {
          $propertySizeSummary = $propertySizeSummary + $articles[$i]->articleSummary['totalSize'];
          $propertyAreaSummary = $propertyAreaSummary + floatval($articles[$i]->getArea());
          $propertyAreaValueSummary = $propertyAreaValueSummary + $articles[$i]->articleSummary['areaValue'];
        }
        $property->propertySizeSummary = $propertySizeSummary;
        $property->propertyAreaSummary = $propertyAreaSummary;
        $property->propertyAreaValueSummary = $propertyAreaValueSummary;

        return $this->render('properties/'.$locale.'.show.html.twig', ['articles' => $articles,
        'property' => $property,
        'importedArticles' => $importedArticles,
        'importedArticlesPaginated' => $importedArticlesPaginated]);

        //return $this->render('articles/show.html.twig', array('article' =>$article));

    }

    /**
     * @Route("/{locale}/property/update/{id}", methods={"GET"})
     */
    public function update(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $property = $this->getDoctrine()->getRepository(Property::class)->find($id);

        $propertyUserId = $property->getUser()->getId();
        $userId = $security->getUser()->getId();
		/*
        if ($propertyUserId !== $userId) {
          throw $this->createNotFoundException(
            'No property found for this user!'
          );
        }
		*/
        $property->userId = $property->getUser()->getId();
        $property->username = $property->getUser()->getUsername();

        return $this->render('properties/'.$locale.'.update.html.twig',array(
            'property' => $property ));
    }

    /**
     * @Route("/{locale}/property/update/{id}", methods={"POST"})
     */
    public function updatePost(Security $security, Request $request, $locale, $id) {
/*
      return new JsonResponse(array(
      'user_id' => $username = $security->getUser()->getId(),
      'address' => preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('address')),
      'identifier' => $request->get('identifier')
      ));
*/
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository(Property::class)->find($id);

        if (!$property) {
          return new JsonResponse(array('message' => 'No property found for id '.$id));
        }

        $propertyUserId = $property->getUser()->getId();
        $userId = $security->getUser()->getId();
		/*
        if ($propertyUserId !== $userId) {
          throw $this->createNotFoundException(
            'No property found for this user!'
          );
        }
		*/
        $property->setAddress($request->get('address'));
        $property->setIdentifier($request->get('identifier'));
        $property->setOwner($request->get('owner'));

        $em->flush();

        return $this->redirect('/'.$locale.'/property/'.$id);
        //return $this->redirect('/'.$locale.'/property/show/'.$id);
    }

    /**
     * @Route ("/{$locale}/property/delete/{id}", methods={"GET"})
     */
    public function delete(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository(Property::class)->find($id);

        $propertyUserId = $property->getUser()->getId();
        $userId = $security->getUser()->getId();
		
        return new JsonResponse(array('message' => 'Deletion not implemented yet!'));

        $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);

        foreach ($articles = $property->getArticles() as $i => $product) {
          $articleService->deleteArticle($articles[$i]->getId());
        }

        $propertyService = new ServiceProperty($this->getDoctrine()->getManager(),Property::class);
        $propertyService->deleteProperty($id);

        return $this->redirect('/'.$locale.'/property/');
    }
}
