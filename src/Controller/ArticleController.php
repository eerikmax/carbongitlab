<?php
namespace App\Controller;

use App\Entity\Article;
use App\Entity\Property;
use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Service\ArticleService as ServiceArticle;


class ArticleController extends AbstractController {

    private function assertLocale($locale) {
      if ($locale !== 'fi' && $locale !== 'en') {
        return $this->redirect('/');
      }
    }

    /**
     * @Route("/{locale}/article/uploadxml", methods={"GET"})
     */
    public function uploadXml(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
        if ($security->getUser()->getRole() == 'super-user') {

          return $this->render('articles/'.$locale.'.uploadxml.html.twig', array
          ('factors' => [], 'showNewFactors' => false));

        } else {
          return $this->redirectToRoute('welcome');
        }
      } else {
        return $this->redirectToRoute('welcome');
      }

    }

    /**
     * @Route("/{locale}/article/uploadxml", methods={"POST"})
     */
    public function uploadXmlPost(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
        if (isset($_FILES["xml"])) {

          if (isset($_FILES['xml']) && ($_FILES['xml']['error'] == UPLOAD_ERR_OK)) {
              $xml = simplexml_load_file($_FILES['xml']['tmp_name']);
              $str = $xml->asXML();

              $xml1 = json_decode(json_encode((array) simplexml_load_string($str)), 1);

              $allParams = $request->request->all();
              if (isset($allParams['addNewProperty'])) {
                return new JsonResponse(array('message' => $allParams));
              }

                return new JsonResponse(array('message' => $xml1));

          }

        }

      } else {
        return $this->redirectToRoute('welcome');
      }

    }

    /**
     * @Route("/article/test", methods={"GET"})
     */

    public function test(Security $security, Request $request) {
/*
      $em = $this->getDoctrine()->getManager();
      $jsonResult = $em->getRepository(Article::class)
      ->createQueryBuilder('u')
      ->where('u.id = 1')
      ->getQuery()
      ->getArrayResult();

      return new JsonResponse($jsonResult);
*/
      $company = $this->getDoctrine()->getRepository(Company::class)->find(2);
      $user = $this->getDoctrine()->getRepository(User::class)->find(10);

      $company->addUser($user);

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($company);
      $entityManager->flush();

      return new JsonResponse(array(
      'done' => 'done'));

    }

    /**
     * @Route("/{locale}/article", name="article_list", methods={"GET"})
     */
    public function article(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
          $userId = $security->getUser()->getId();
          //$user = $this->getDoctrine()->getRepository(User::class)->find($userId);
          //$articles = $user->getArticles();
          $em = $this->getDoctrine()->getManager();
          $articles = $em->getRepository(Article::class)
          ->createQueryBuilder('u')
          ->where('u.user = '.$userId)
          ->getQuery()
          ->getResult();

          foreach ($articles as $i => $product) {
             $articles[$i]->propertyId = $articles[$i]->getProperty()->getId();

             $totalAreaSum = intval($articles[$i]->getMat()) +
             intval($articles[$i]->getKut()) +
             intval($articles[$i]->getKot()) +
             intval($articles[$i]->getMut()) +
             intval($articles[$i]->getMak()) +
             intval($articles[$i]->getKuk()) +
             intval($articles[$i]->getKok()) +
             intval($articles[$i]->getMuk()) +
             intval($articles[$i]->getEnp());

             $articleSummary = array(
               'totalArea'=> $totalAreaSum,
               'propertyAddress'=> $articles[$i]->getProperty()->getAddress(),
               'areaValue' => 982
             );

             $articles[$i]->articleSummary = $articleSummary;
          }

          return $this->render('articles/index.html.twig', array
          ('articles' => $articles));
        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    //NEW ARTICLE /newarticle/propertyId
    /**
     * @Route ("/{locale}/newarticle/property/{propertyId}/", methods={"GET"})
     */
    public function newFromProperty(Request $request, $locale, $propertyId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $newArticle = new Article();

        $property = $this->getDoctrine()->getRepository(Property::class)->find($propertyId);

        $allArticles  = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articles = $allArticles->getAllArticles();

        return $this->render('articles/'.$locale.'.new.html.twig',array(
            'article'=>$newArticle, 'property'=>$property, 'propertyId'=>$propertyId, 'articles' => $articles
        ));
    }

    /**
     * @Route ("/{locale}/newarticle/property/{propertyId}/", methods={"POST"})
     */
    public function newFromPropertyPost(Security $security, Request $request, $locale, $propertyId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $article = new Article();

        $user = $security->getUser();
        $property = $this->getDoctrine()->getRepository(Property::class)->find($propertyId);

        $article->setArea(floatval($request->get('area')));
        $article->setPlace(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('place')));
        $article->setHeight(floatval($request->get('height')));
        $article->setSort(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('sort')));
        $article->setAge(intval($request->get('age')));
        $article->setDevelopment_class(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('development_class')));
        $article->setMat(intval($request->get('mat')));
        $article->setKut(intval($request->get('kut')));
        $article->setKot(intval($request->get('kot')));
        $article->setMut(intval($request->get('mut')));
        $article->setMak(intval($request->get('mak')));
        $article->setKuk(intval($request->get('kuk')));
        $article->setKok(intval($request->get('kok')));
        $article->setMuk(intval($request->get('muk')));
        $article->setEnp(intval($request->get('enp')));
        $article->setProperty($property);
        $article->setUser($security->getUser());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

        return $this->redirect('/'.$locale.'/property/'.$propertyId);
    }

    // Delete later
    /**
     * @Route ("/newarticle", methods={"GET"})
     */
    public function new(Request $request) {
        $newArticle = new Article();

        $allArticles  = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articles = $allArticles->getAllArticles();

        return $this->render('articles/new.html.twig',array(
            'article'=>$newArticle, 'articles' => $articles
        ));
    }

    /**
     * @Route ("/newarticle", methods={"POST"})
     */
    public function newPost(Security $security, Request $request) {

        $article = new Article();

        $user = $security->getUser();

        $article->setArea(floatval($request->get('area')));
        $article->setPlace(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('place')));
        $article->setHeight(floatval($request->get('height')));
        $article->setSort(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('sort')));
        $article->setAge(intval($request->get('age')));
        $article->setDevelopment_class(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('development_class')));
        $article->setMat(intval($request->get('mat')));
        $article->setKut(intval($request->get('kut')));
        $article->setKot(intval($request->get('kot')));
        $article->setMut(intval($request->get('mut')));
        $article->setMak(intval($request->get('mak')));
        $article->setKuk(intval($request->get('kuk')));
        $article->setKok(intval($request->get('kok')));
        $article->setMuk(intval($request->get('muk')));
        $article->setEnp(intval($request->get('enp')));
        $article->setUser($security->getUser());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

        return $this->redirectToRoute('article_list');
    }

    /**
     * @Route ("/article/{id}", name= "article_show", methods={"GET"})
     */
    public function show(Security $security, $id) {
        //service example: $article = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        //service example: $article = $article->getArticle($id);
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $allArticles  = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articles = $allArticles->getAllArticles();

        return $this->render('articles/show.html.twig', ['articles' => $articles, 'article' =>$article]);

        //return $this->render('articles/show.html.twig', array('article' =>$article));

    }

    /**
     * @Route ("/property/{propertyId}/article/{articleId}", name= "article_showFromProperty", methods={"GET"})
     */
    public function showFromProperty(Security $security, $propertyId, $articleId) {
        $article  = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $article = $article->getArticle($articleId);

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $allArticles  = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articles = $allArticles->getAllArticles();

        return $this->render('articles/show.html.twig', ['articles' => $articles, 'article' =>$article]);

        //return $this->render('articles/show.html.twig', array('article' =>$article));

    }

    /**
     * @Route("/article/update/{id}", methods={"GET"})
     */
    public function update(Security $security, Request $request, $id) {

        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $articleSummary = array(
          'propertyAddress'=> $article->getProperty()->getAddress()
        );

        $article->articleSummary = $articleSummary;

        $allArticles  = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articles = $allArticles->getAllArticles();

        return $this->render('articles/update.html.twig',array(
            'article' => $article, 'articles' => $articles
        ));
    }

    /**
     * @Route("/{locale}/property/{propertyId}/article/update/{articleId}", methods={"GET"})
     */
    public function updateFromProperty(Security $security, Request $request, $locale, $propertyId, $articleId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $article = $this->getDoctrine()->getRepository(Article::class)->find($articleId);

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $property = $this->getDoctrine()->getRepository(Property::class)->find($propertyId);

        $articleSummary = array(
          'propertyAddress'=> $article->getProperty()->getAddress()
        );

        $article->articleSummary = $articleSummary;

        $allArticles  = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articles = $allArticles->getAllArticles();

        return $this->render('articles/'.$locale.'.update.html.twig',array(
            'article' => $article, 'property' => $property, 'articles' => $articles
        ));
    }

    /**
     * @Route("/{locale}/property/{propertyId}/article/update/{articleId}", methods={"POST"})
     */
    public function updatePost(Security $security, Request $request, $locale, $propertyId, $articleId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($articleId);

        if (!$article) {
          throw $this->createNotFoundException(
            'No article found for id '.$id
          );
        }

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $newName = $request->request->get('username');
        $article->setPlace($newName);
        $article->setArea(floatval($request->get('area')));
        $article->setPlace(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('place')));
        $article->setHeight(floatval($request->get('height')));
        $article->setSort(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('sort')));
        $article->setAge(intval($request->get('age')));
        $article->setDevelopment_class(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('development_class')));
        $article->setMat(intval($request->get('mat')));
        $article->setKut(intval($request->get('kut')));
        $article->setKot(intval($request->get('kot')));
        $article->setMut(intval($request->get('mut')));
        $article->setMak(intval($request->get('mak')));
        $article->setKuk(intval($request->get('kuk')));
        $article->setKok(intval($request->get('kok')));
        $article->setMuk(intval($request->get('muk')));
        $article->setEnp(intval($request->get('enp')));

        $em->flush();

        $propertyId = $article->getProperty()->getId();
        return $this->redirect('/'.$locale.'/property/'.$propertyId);
    }
    //this route might be useless
    /**
     * @Route("/property/{propertyId}/article/update/{articleId}", methods={"POST"})
     */
    public function updateFromPropertyPost(Security $security, Request $request, $propertyId, $articleId) {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($articleId);

        if (!$article) {
          throw $this->createNotFoundException(
            'No article found for id '.$id
          );
        }

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $newName = $request->request->get('username');
        $article->setPlace($newName);
        $article->setArea(floatval($request->get('area')));
        $article->setPlace(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('place')));
        $article->setHeight(floatval($request->get('height')));
        $article->setSort(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('sort')));
        $article->setAge(intval($request->get('age')));
        $article->setDevelopment_class(preg_replace("/[^a-zA-ZäöåÄÖÅ0-9]/", "", $request->get('development_class')));
        $article->setMat(intval($request->get('mat')));
        $article->setKut(intval($request->get('kut')));
        $article->setKot(intval($request->get('kot')));
        $article->setMut(intval($request->get('mut')));
        $article->setMak(intval($request->get('mak')));
        $article->setKuk(intval($request->get('kuk')));
        $article->setKok(intval($request->get('kok')));
        $article->setMuk(intval($request->get('muk')));
        $article->setEnp(intval($request->get('enp')));

        $em->flush();

        $propertyId = $article->getProperty()->getId();
        return $this->redirect('/property/'.$propertyId);
    }

    /**
     * @Route ("/article/delete/{id}", methods={"GET"})
     */
    public function delete(Security $security, Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articleService->deleteArticle($id);

        return $this->redirectToRoute('property_list');
    }

    /**
     * @Route ("/property/{propertyId}/article/delete/{articleId}", methods={"GET"})
     */
    public function deleteFromProperty(Security $security, Request $request, $propertyId, $articleId) {

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($articleId);

        $articleUserId = $article->getUser()->getId();
        $userId = $security->getUser()->getId();

        if ($articleUserId !== $userId) {
          throw $this->createNotFoundException(
            'No article found for this user!'
          );
        }

        $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
        $articleService->deleteArticle($articleId);

        return $this->redirect('/property/'.$propertyId);
    }
}
