<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use App\Entity\Question;
use App\Entity\Factor;
use App\Entity\Answer;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
//services can have more functions
//service example: $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
//service example: $article = $articleService->getArticle($id);
use App\Service\AnswerService as ServiceAnswer;
use App\Service\UserService as ServiceUser;
use App\Service\PropertyService as ServiceProperty;
use App\Service\CompanyService as CompanyService;
use App\Service\QuestionService as QuestionService;

class ResultController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  /**
   * @Route("/{locale}/questionresults/chart/{chartType}/{companyId}/{formName}/", methods={"GET"})
   */
  public function questionResultsChart(Security $security, Request $request, PaginatorInterface $paginator, $chartType, $locale, $companyId, $formName) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

        if ($chartType !== 'linechart' && $chartType !== 'stackedchart') {
          return new JsonResponse(array('message' => 'No "'.$chartType.'" chart'));
        }

        $userId = $security->getUser()->getId();
        $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
        $hasPermission = $companyService->checkPermission($companyId, $userId);
        if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

        $entityManager = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM answer WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        $answerResults = $stmt->fetchAll();
        if (!$answerResults) { return new JsonResponse(array('message' => 'No answer results!')); }

        $labelsData = array();
        $addedInArray = array();
        $factorsData = array();
        $tabsArray = array();
        foreach ($answerResults as $i => $product) {
          if (!in_array($answerResults[$i]['tab_name'], $addedInArray)) {
            array_push($labelsData, $answerResults[$i]['tab_name']);
            array_push($addedInArray, $answerResults[$i]['tab_name']);
          }
          if (!in_array($answerResults[$i]['factor_id'], $addedInArray)) {
            array_push($factorsData, array('name' => $answerResults[$i]['factor_id']));
            array_push($addedInArray, $answerResults[$i]['factor_id']);
          }
          array_push($tabsArray, $answerResults[$i]['tab_name']);
        }

        $tabsArray = array_unique($tabsArray);

        $sql = "SELECT COUNT(DISTINCT(factor_id)) as factorCount FROM answer WHERE company_id = '".$companyId."' AND form_name = '".$formName."' ".
        "AND tab_name = '".$tabsArray[0]."';";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        $countResults = $stmt->fetch();
        if ($countResults) {

        }
        //return new JsonResponse(array('message' => $countResults['factorCount']));

        $factorCount = $countResults['factorCount'];

        $newResultsArray = array();
        foreach ($tabsArray as $i => $product) {
          $sql = "SELECT COUNT(DISTINCT(factor_id)) as factorCount FROM answer WHERE company_id = '".$companyId."' AND form_name = '".$formName."' ".
          "AND tab_name = '".$tabsArray[$i]."';";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabFactorCountResults = $stmt->fetch();
          if ($factorCount < 2 || $tabFactorCountResults['factorCount'] !== $factorCount) {
            return new JsonResponse(array('message' => 'Not enough factors (more than 1) or the tabs do not have the same factors/questions!'));
          }

          $sql = "SELECT * FROM answer WHERE company_id = '".$companyId."' AND form_name = '".$formName."' ".
          "AND tab_name = '".$tabsArray[$i]."' ORDER BY time DESC LIMIT ".$factorCount.";";

          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $newResults = $stmt->fetchAll();
          if ($newResults) {
            array_push($newResultsArray, $newResults);
          }
        }

        $newArrays = array();

        foreach ($newResultsArray as $i => $product) {

          foreach ($newResultsArray[$i] as $k => $product) {
            if (!isset($newResultsArray[$i]['totalSum'])) {
              $newResultsArray[$i]['totalSum'] = 0;
            }
            $sql = "SELECT * FROM question WHERE id = '".$newResultsArray[$i][$k]['question_id']."';";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $questionResults = $stmt->fetch();
            $newResultsArray[$i][$k]['questionData'] = $questionResults;

            $sql = "SELECT * FROM factor WHERE id = '".$newResultsArray[$i][$k]['factor_id']."';";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $factorResults = $stmt->fetch();
            $newResultsArray[$i][$k]['factorData'] = $factorResults;

            $newResultsArray[$i][$k]['result'] = round($newResultsArray[$i][$k]['content'] * $newResultsArray[$i][$k]['factorData']['value'], 3);
            $newResultsArray[$i]['tabName'] = $newResultsArray[$i][0]['tab_name'];
            $newResultsArray[$i]['totalSum'] = $newResultsArray[$i]['totalSum'] + $newResultsArray[$i][$k]['result'];

            $newItem = array('name' => $newResultsArray[$i][$k]['factorData']['id'],
            'result' => $newResultsArray[$i][$k]['result']);
            // might break if more than 1 factor_id on the same tab !!!
            //if (!array_key_exists($newResultsArray[$i][$k]['factorData']['id'], $newArrays)) {
              array_push($newArrays, $newItem);
            // }

          }
        }

        $tabNamesAndTotalSumsArray = [];
        foreach ($newResultsArray as $i => $product) {
            array_push($tabNamesAndTotalSumsArray, array('tabName' => $newResultsArray[$i]['tabName'],
            'totalSum' => $newResultsArray[$i]['totalSum']));
        }

        /*
        function sameCount(array $array) : bool {
          $lastCount = null;

          foreach ($array as $sub) {
            $count = count($sub);
            if (null !== $lastCount && $lastCount !== $count) {
                return false;
            }
            $lastCount = $count;
          }

          return true;
        }
        */
        $label = [];
        $collector = [];
        $datasetsData  = [];
        foreach ($newArrays as $subarray) {
            $label[$subarray['name']] = $subarray['name'];
            $collector[$subarray['name']][] = $subarray['result'];
        }

        foreach($label as $i) {
            $sql = "SELECT title_fi, title_en FROM factor WHERE id = '".$label[$i]."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $factorNameResults = $stmt->fetch();
            if ($locale === 'fi') {
              $newTitle = $factorNameResults['title_fi'];
              $newTitle = str_replace("(litra)", "tCO2 (eq)", $newTitle);
              $newTitle = str_replace("(kilogramma)", "tCO2 (eq)", $newTitle);
              $newTitle = str_replace("(kilogrammaa)", "tCO2 (eq)", $newTitle);
            } else {
              $newTitle = $factorNameResults['title_en'];
              $newTitle = str_replace("(liter)", "tCO2 (eq)", $newTitle);
              $newTitle = str_replace("(kilograms)", "tCO2 (eq)", $newTitle);
            }
            $datasetsData[] = ['label' => $newTitle, 'data' => $collector[$i], 'borderWidth' => 2];
            $datasetsData1[] = ['name' => $newTitle, 'data' => $collector[$i]];
        }

        $jsonedLabelsData = json_encode($labelsData);
/*
        $datasetsData2 = array([
          'name' => 'Diesel (tCO2)',
          'data' => [8.096, 6.458, 5.85, 7.488, 6.458, 8.845]
        ],
        [
          'name' => 'Gasoline (tCO2)',
          'data' => [6.976, 8.742, 7.543, 5.45, 6.54, 11.009]
        ],
        [
          'name' => 'Biogas (tCO2 eq)',
          'data' => [0.15, 1.763, 0.128, 3.863, 0.098, 3.094]
        ]);
*/
        $output = array_reduce($datasetsData1, function (array $carry, array $item) : array {
              foreach($item['data'] as $key => $datum) {
                  $carry[$key][] = $datum;
              } return $carry; }, []);
        //return new JsonResponse(array('message' => $output));

        $jsonedDatasets = json_encode($datasetsData);
        $jsonedDatasets1 = json_encode($datasetsData1);

        return $this->render('questionresults/'.$locale.'.'.$chartType.'.html.twig', array(
            'companyId' => $companyId,
            'formName' => $formName,
            'filteredBy' => true,
            'jsonedDatasets' => $jsonedDatasets,
            'jsonedLabels' => $jsonedLabelsData,
            'jsonedDatasets1' => $jsonedDatasets1,
            'tabNamesAndTotalSumsArray' => $tabNamesAndTotalSumsArray));

      } else {
        return $this->redirectToRoute('welcome');
      }

  }

  /**
   * @Route("/{locale}/questionresults-year/{year}/{companyId}/", methods={"GET"})
   */
  public function questionResultsByYear(Security $security, Request $request, PaginatorInterface $paginator, $locale, $year, $companyId) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

        $userId = $security->getUser()->getId();
        $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
        $hasPermission = $companyService->checkPermission($companyId, $userId);
        if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

        $results = array();
        // Paginate the results of the query
        $resultsPaginated = $paginator->paginate(
            // Doctrine Query, not results
            $results,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );

        return $this->render('questionform-answer/'.$locale.'.show-answers-by-year.html.twig', array(
            'filteredBy' => true,
            'formName' => 'formName',
            'resultsPaginated' => $resultsPaginated));

      } else {
        return $this->redirectToRoute('welcome');
      }

  }

    /**
     * @Route("/{locale}/questionresults/{companyId}/{formName}/", methods={"GET"})
     */
    public function questionFormResultsForm(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $userId = $security->getUser()->getId();
          $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
          $hasPermission = $companyService->checkPermission($companyId, $userId);
          if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          $companyName = $company->getName();

          if ($formName) {
            $entityManager = $this->getDoctrine()->getManager();
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

            $tabsArray = array();
            foreach ($tabsResults as $i => $product) {
              array_push($tabsArray, $tabsResults[$i]['tab_name']);
            }
            $tabsArray = array_unique($tabsArray);
          } else {
            $formName = '';
          }

          $selectedTabName = array_shift($tabsArray);
          $selectedTabObj = (object)[];
          $selectedTabObj->tab_name = $selectedTabName;

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."' AND tab_name = '".$selectedTabName."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabQuestionResults = $stmt->fetchAll();
          if (!$tabQuestionResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          return $this->redirect('/'.$locale.'/questionresults/'.$companyId.'/'.$formName.'/'.$selectedTabObj->tab_name);

          /*
          return $this->render('questionform-answer/'.$locale.'.index.html.twig', array(
              'companyId' => $companyId,
              'companyName' => $companyName,
              'formName' => $formName,
              'selectedTab' => $selectedTabObj,
              'tabNames' => $tabsArray,
              'tabQuestions' => $tabQuestionResults ));
        */
        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionresults/{companyId}/{formName}/{tabName}/", methods={"GET"})
     */
    public function questionFormResultsFormTab(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $targetTime = $request->get('targetTime');
          if (!$targetTime) { $targetTime = time(); }

          $targetTimeFormatted = date('Y-m-d', $targetTime);

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          $companyName = $company->getName();
          $entityManager = $this->getDoctrine()->getManager();

          if ($formName) {
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

            $tabsArray = array();
            $selectedTab = false;
            foreach ($tabsResults as $i => $product) {
            array_push($tabsArray, $tabsResults[$i]['tab_name']);
             if ($tabsResults[$i]['tab_name'] === $tabName) {
                $selectedTab = $tabsResults[$i];
              }
            }
            $tabsArray = array_unique($tabsArray);
          } else {
            $formName = '';
          }

          $sql = "SELECT COUNT(DISTINCT(factor_id)) as factorCount FROM answer WHERE company_id = '".$companyId."' AND form_name = '".$formName."' ".
          "AND tab_name = '".$tabsArray[0]."';";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $countResults = $stmt->fetch();
          if ($countResults) {
          }

          $factorCount = $countResults['factorCount'];
          $showChart = true;

          foreach ($tabsArray as $i => $product) {
            $sql = "SELECT COUNT(DISTINCT(factor_id)) as factorCount FROM answer WHERE company_id = '".$companyId."' AND form_name = '".$formName."' ".
            "AND tab_name = '".$tabsArray[$i]."';";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabFactorCountResults = $stmt->fetch();
            if ($factorCount < 2 || $tabFactorCountResults['factorCount'] !== $factorCount) {
              $showChart = false;
            }
          }

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."' AND tab_name = '".$selectedTab['tab_name']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabQuestionResults = $stmt->fetchAll();
          if (!$tabQuestionResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          foreach ($tabQuestionResults as $i => $product) {
              $sql = "SELECT * FROM factor WHERE id = '".$tabQuestionResults[$i]['factor_id']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $factorResults = $stmt->fetch();
              if (!$factorResults) { return new JsonResponse(array('message' => 'No factor results!')); }
              $tabQuestionResults[$i]['factor'] = $factorResults;
              $tabQuestionResults[$i]['factor']['valueFloated'] = (float)$factorResults['value'];

              $sql = "SELECT time, content, target_time, user_id FROM answer WHERE question_id = '".$tabQuestionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
              "AND tab_name = '".$selectedTab['tab_name']."' ORDER BY time DESC;";
              //$sql = "SELECT time, content FROM answer WHERE question_id = '".$tabQuestionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
              //"AND tab_name = '".$selectedTab['tab_name']."' AND time >= DATE(NOW()) - INTERVAL 7 DAY ORDER BY time DESC;";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $answerResults = $stmt->fetch();

              if ($answerResults) {
                $sql = "SELECT username, email FROM user WHERE id = '".$answerResults['user_id']."'";
                $stmt = $entityManager->getConnection()->prepare($sql);
                $stmt->execute();
                $userResults = $stmt->fetch();
                if (!$userResults) { return new JsonResponse(array('message' => 'No user results!')); }

                $tabQuestionResults[$i]['answer'] = $answerResults['content'];

                $tabQuestionResults[$i]['result'] = round($tabQuestionResults[$i]['answer'] * $tabQuestionResults[$i]['factor']['value'], 3);

                $tabQuestionResults[$i]['time'] = $answerResults['time'];
                $tabQuestionResults[$i]['user'] = $userResults;

                $tabQuestionResults[$i]['timeFi'] = date('d.m.Y', $answerResults['time']);
                $tabQuestionResults[$i]['timeUk'] = date('d/m/Y', $answerResults['time']);
                //$tabQuestionResults[$i]['targetTimeFi'] = date('d.m.Y', $answerResults['target_time']);
                //$tabQuestionResults[$i]['targetTimeUk'] = date('d/m/Y', $answerResults['target_time']);
                /* "Month or year" start */
                $monthEnd = strtotime('last day of this month', $answerResults['target_time']);
                $monthEndFormatted = date('d.m.Y', $monthEnd);
                if ($monthEndFormatted === date('d.m.Y', $answerResults['target_time'])) {
                  $tabQuestionResults[$i]['targetTimeFi'] = date('Y', $answerResults['target_time']);
                  $tabQuestionResults[$i]['targetTimeUk'] = date('Y', $answerResults['target_time']);
                } else {
                  $tabQuestionResults[$i]['targetTimeFi'] = date('m/Y', $answerResults['target_time']);
                  $tabQuestionResults[$i]['targetTimeUk'] = date('m/Y', $answerResults['target_time']);
                }
                /* "Month or year" end */
              } else {
                $tabQuestionResults[$i]['time'] = '';
                $tabQuestionResults[$i]['answer'] = '';
                $tabQuestionResults[$i]['result'] = 0;
              }
          }

          foreach ($tabsArray as $i => $product) {
            if ($tabsArray[$i] === $tabName) {
              //unset($tabsArray[$i]);
            }
          }


          $labelsArray = array();
          foreach ($tabQuestionResults as $i => $product) {
            if ($locale === 'fi') {
              $newTitle = $tabQuestionResults[$i]['factor']['title_fi'];
              $newTitle = str_replace(" (litra)", ': '.$tabQuestionResults[$i]['result'], $newTitle);
              $newTitle = str_replace(" (kilogramma)", ': '.$tabQuestionResults[$i]['result'], $newTitle);
              $newTitle = str_replace(" (kilogrammaa)", ': '.$tabQuestionResults[$i]['result'], $newTitle);
              array_push($labelsArray, $newTitle." tCO2 (eq)");
            } else {
              $newTitle = $tabQuestionResults[$i]['factor']['title_en'];
              $newTitle = str_replace(" (liter)", ': '.$tabQuestionResults[$i]['result'], $newTitle);
              $newTitle = str_replace(" (kilograms)", ': '.$tabQuestionResults[$i]['result'], $newTitle);
              array_push($labelsArray, $newTitle." tCO2 (eq)");
            }
          }
          $jsonedLabelsArray = json_encode($labelsArray);

          $valuesArray = array();
          foreach ($tabQuestionResults as $i => $product) {
              array_push($valuesArray, $tabQuestionResults[$i]['result']);
          }
          //return new JsonResponse(array('message' => $tabQuestionResults));
          $jsonedValuesArray = json_encode($valuesArray);
          //to do: values for the pie


          return $this->render('questionresults/'.$locale.'.index.html.twig', array(
              'showChart' => $showChart,
              'companyId' => $companyId,
              'companyName' => $companyName,
              'targetTime' => $targetTime,
              'targetTimeFormatted' => $targetTimeFormatted,
              'formName' => $formName,
              'selectedTab' => $selectedTab,
              'tabNames' => $tabsArray,
              'tabQuestions' => $tabQuestionResults,
              'jsonedLabelsArray' => $jsonedLabelsArray,
              'jsonedValuesArray' => $jsonedValuesArray));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/answer/{companyId}/{formName}/{tabName}/", methods={"POST"})
     */
    public function questionFormAnswerFormTabPost(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $allParams = $request->request->all();
          $answers = $allParams['answers'];
          $targetTime = $allParams['targetTime'];

          $user = $security->getUser();
          $entityManager = $this->getDoctrine()->getManager();

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'No company!')); }

          foreach ($answers as $i => $product) {
            //echo $answers[$i]['content'];

            $answer = new Answer();
            $question = $this->getDoctrine()->getRepository(Question::class)->find($answers[$i]['question-id']);
            if (!$question) { return new JsonResponse(array('message' => 'No question!')); }

            $factor = $question->getFactor();
            if (!$question) { return new JsonResponse(array('message' => 'No factor!')); }

            $answer->setContent($answers[$i]['content']);
            $answer->setFormName($formName);
            $answer->setTabName($tabName);
            $answer->setQuestion($question);
            $answer->setUser($user);
            $answer->setCompany($company);
            $answer->setFactor($factor);
            $answer->setTargetTime(strtotime($targetTime));
            $answer->setTime(time());

            $entityManager->persist($answer);
            $entityManager->flush();
          }

          $this->addFlash('notice',
            'Vastaukset päivitetty onnistuneesti. Voit vastata lomakkeen muihin välilehtiin.');
          return $this->redirect('/'.$locale.'/questionform/answer/'.$companyId.'/'.$formName.'/'.$tabName.'/'.'?targetTime='.strtotime($targetTime));
          //return new JsonResponse(array('message' => $answers));

        } else {
          return $this->redirectToRoute('welcome');
        }
    }

    /**
     * @Route("/{locale}/questionform/showanswers/{companyId}/{formName}/", methods={"GET"})
   */
    public function questionFormShowAnswers(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          //return new JsonResponse(array('message' => 'questionFormShowAnswers'));
          $filteredBy = $request->get('filteredBy');
          if (!$filteredBy) { $filteredBy = 'all'; }

          $targetTime = $request->get('targetTime');
          if (!$targetTime) { $targetTime = time(); }

          $targetTimeFormatted = date('Y-m-d', $targetTime);

          $entityManager = $this->getDoctrine()->getManager();

          if ($formName) {
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          } else {
            $formName = '';
          }

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          //rename tabQuestions

          $questionResults = $stmt->fetchAll();
          if (!$questionResults) { return new JsonResponse(array('message' => 'No results!')); }

          $enableSubmit = false;
          $answers = array();
          $noAnswers = array();
          foreach ($questionResults as $i => $product) {
            $sql = "SELECT id, title_fi, title_en, value, measure FROM factor WHERE id = '".$questionResults[$i]['factor_id']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $factorResults = $stmt->fetch();
            if (!$factorResults) { return new JsonResponse(array('message' => 'No factor results!')); }
            $questionResults[$i]['factor'] = $factorResults;
            $questionResults[$i]['factor']['valueFloated'] = (float)$factorResults['value'];

            $sql = "SELECT id, user_id, time, content, question_id, target_time FROM answer WHERE question_id = '".$questionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
            "ORDER BY time DESC;";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $answerResults = $stmt->fetch();
            if ($answerResults) {
              $sql = "SELECT id, name, username, email FROM user WHERE id = '".$answerResults['user_id']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $userResults = $stmt->fetch();
              if (!$userResults) { return new JsonResponse(array('message' => 'No user results!')); }
              $questionResults[$i]['user'] = $userResults;

              $questionResults[$i]['time'] = $answerResults['time'];
              $questionResults[$i]['answer'] = $answerResults['content'];
              $questionResults[$i]['question_id'] = $answerResults['id'];

              $questionResults[$i]['timeFi'] = date('d.m.Y', $answerResults['time']);
              $questionResults[$i]['timeUk'] = date('d/m/Y', $answerResults['time']);
              $monthEnd = strtotime('last day of this month', $answerResults['time']);
              $monthEndFormatted = date('d.m.Y', $monthEnd);
              if ($monthEndFormatted === date('d.m.Y', $answerResults['target_time'])) {
                $questionResults[$i]['targetTimeFi'] = date('Y', $answerResults['target_time']);
                $questionResults[$i]['targetTimeUk'] = date('Y', $answerResults['target_time']);
              } else {
                $questionResults[$i]['targetTimeFi'] = date('m/Y', $answerResults['target_time']);
                $questionResults[$i]['targetTimeUk'] = date('m/Y', $answerResults['target_time']);
              }
              array_push($answers, $questionResults[$i]);
            } else {
              $questionResults[$i]['time'] = '';
              $questionResults[$i]['answer'] = 'No answer';
              $enableSubmit = true;
              array_push($noAnswers, $questionResults[$i]);
            }
          }

          if ($filteredBy !== 'all') {
            $newAnswers = array();
            foreach ($answers as $i => $value) {
                if ($answers[$i]['tab_name'] == $filteredBy)  {
                    $newAnswers[] = $answers[$i];
                }
            }
            $answers = $newAnswers;
          }

          /*
          TABS
          */
          $notAnsweredTabsArray = array();
          $notAnsweredQuestions = array();
          $addedInArray = array();
          foreach ($noAnswers as $i => $product) {
            if (!in_array($noAnswers[$i]['tab_name'], $addedInArray)) {
              array_push($notAnsweredTabsArray, $noAnswers[$i]);
              array_push($addedInArray, $noAnswers[$i]['tab_name']);
            }
          }

          foreach ($noAnswers as $i => $product) {
            foreach ($notAnsweredTabsArray as $o => $product) {
              if ($noAnswers[$i]['tab_name'] == $notAnsweredTabsArray[$o]['tab_name']) {
                if (!isset($notAnsweredTabsArray[$o]['questions'])) {
                  $notAnsweredTabsArray[$o]['questions'] = array();
                }
                /*
                $noAnswers[$i]['targetTimeFi'] = 'test';
                $monthEnd = strtotime('last day of this month', $noAnswers[$i]['target_time']);
                $monthEndFormatted = date('d.m.Y', $monthEnd);
                if ($monthEndFormatted === date('d.m.Y', $noAnswers[$i]['target_time'])) {
                  $noAnswers[$i]['targetTimeFi'] = date('Y', $noAnswers[$i]['target_time']);
                  $noAnswers[$i]['targetTimeUk'] = date('Y', $noAnswers[$i]['target_time']);
                } else {
                  $noAnswers[$i]['targetTimeFi'] = date('m/Y', $noAnswers[$i]['target_time']);
                  $noAnswers[$i]['targetTimeUk'] = date('m/Y', $noAnswers[$i]['target_time']);
                }
                */
                //$noAnswers[$i]['timeFi'] = date('d.m.Y', $noAnswers[$i]['time']);
                //$noAnswers[$i]['timeUk'] = date('d/m/Y', $noAnswers[$i]['time']);
                array_push($notAnsweredTabsArray[$o]['questions'], $noAnswers[$i]);
              }
            }
          }

          // Paginate the results of the query
          $answersPaginated = $paginator->paginate(
              // Doctrine Query, not results
              $answers,
              // Define the page parameter
              $request->query->getInt('page', 1),
              // Items per page
              5
          );

          $sql = "SELECT tab_name FROM answer WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabsResults = $stmt->fetchAll();
          if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          $answeredTabsArray = array();
          foreach ($tabsResults as $i => $product) {
              array_push($answeredTabsArray, $tabsResults[$i]['tab_name']);
          }
          $answeredTabsArray = array_unique($answeredTabsArray);

          //echo 'filtered by: '.$filteredBy;

          return $this->render('questionform-answer/'.$locale.'.show-answers.html.twig', array(
              'filteredBy' => $filteredBy,
              'targetTime' => $targetTime,
              'targetTimeFormatted' => $targetTimeFormatted,
              'companyId' => $companyId,
              'formName' => $formName,
              'answeredTabsArray' => $answeredTabsArray,
              'answers' => $answers,
              'answersPaginated' => $answersPaginated,
              'noAnswers' => $noAnswers,
              'notAnsweredTabsArray' => $notAnsweredTabsArray,
              'enableSubmit' => $enableSubmit ));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }
}
