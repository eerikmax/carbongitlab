<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use App\Entity\Factor;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\StreamedResponse;

use App\Service\ArticleService as ServiceArticle;
use App\Service\PropertyService as ServiceProperty;
use App\Service\FactorService as ServiceFactor;

class FactorController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  /**
   * @Route("/factor/test", methods={"GET"})
   */
  public function test(Request $request) {

      $property = $this->getDoctrine()->getRepository(Property::class)->find(2);
      $propertyArticles = $property->getArticles();
/*
      $em = $this->getDoctrine()->getManager();
      $jsonResult = $em->getRepository(Property::class)
      ->createQueryBuilder('u')
      ->where('u.id = 2')
      ->getQuery()
      ->getArrayResult();

      return new JsonResponse($jsonResult);
*/
      return new Response(count($propertyArticles).' articles');

  }

  /**
   * @Route("/{locale}/factor/uploadcsv", methods={"GET"})
   */
  public function uploadCsv(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
      if ($security->getUser()->getRole() == 'super-user') {

        return $this->render('factors/'.$locale.'.uploadcsv.html.twig', array
        ('factors' => [], 'showNewFactors' => false));

      } else {
        return $this->redirectToRoute('welcome');
      }
    } else {
      return $this->redirectToRoute('welcome');
    }

  }

  /**
   * @Route("/{locale}/factor/uploadcsv", methods={"POST"})
   */
  public function uploadCsvPost(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
      /*
      if (isset($_FILES["xml"])) {

        if (isset($_FILES['xml']) && ($_FILES['xml']['error'] == UPLOAD_ERR_OK)) {
            $xml = simplexml_load_file($_FILES['xml']['tmp_name']);
            $str = $xml->asXML();

            $xml1 = json_decode(json_encode((array) simplexml_load_string($str)), 1);

            return new JsonResponse(array('message' => $xml1));
        }



      }*/

      if ($security->getUser()->getRole() == 'super-user') {


      if (isset($_FILES["csv"])) {

        $tmpName = $_FILES['csv']['tmp_name'];
        $csvAsArray = array_map('str_getcsv', file($tmpName));

        $uploadedFactorsArray = array();
        foreach ($csvAsArray as $key => $value) {
            $uploadedFactorsArray[] = [
                'id' => $value[0],
                'title_fi' => $value[1],
                'title_en' => $value[2],
                'value' => $value[3],
                'measure' => $value[4],
                'source' => $value[5],
                'reference_id' => $value[6],
                'last_updated' => $value[7]
            ];
        }
        // lastUpdatedFi = date('d.m.Y', $factors[$i]->getLastUpdated()
        //remove first
        unset($uploadedFactorsArray[0]);
        $uploadedFactorsArray = array_values($uploadedFactorsArray);
        //return new JsonResponse(array('message' => $newFactorsArray));

        $newFactors = array();
        $updatedFactors = array();
        $oldFactors = array();

        foreach ($uploadedFactorsArray as $i => $value) {
            if ($uploadedFactorsArray[$i]['reference_id'] == 'emptyreference_id') {
              $uploadedFactorsArray[$i]['reference_id'] = '';
            } if ($uploadedFactorsArray[$i]['source'] == 'emptysource') {
              $uploadedFactorsArray[$i]['source'] = '';
            }
            if ($uploadedFactorsArray[$i]['id'] == '') {
              $uploadedFactorsArray[$i]['last_updated'] = date('d.m.Y', time());
              array_push($newFactors, $uploadedFactorsArray[$i]);
            } if ($uploadedFactorsArray[$i]['last_updated'] == '' && $uploadedFactorsArray[$i]['id'] !== '') {
              $uploadedFactorsArray[$i]['last_updated'] = date('d.m.Y', time());
              array_push($updatedFactors, $uploadedFactorsArray[$i]);
            } if ($uploadedFactorsArray[$i]['last_updated'] !== '' && $uploadedFactorsArray[$i]['id'] !== '') {
              array_push($oldFactors, $uploadedFactorsArray[$i]);
            }
        }

        //return new JsonResponse(array('updatedFactors' => $updatedFactors, 'newFactors' => $newFactors, 'oldFactors' => $oldFactors));

        return $this->render('factors/'.$locale.'.uploadcsv.html.twig', array
        ('newFactors' => $newFactors,
        'updatedFactors' => $updatedFactors,
        'oldFactors' => $oldFactors,
        'showNewFactors' => true));

      } else {

        $entityManager = $this->getDoctrine()->getManager();

        $allParams = $request->request->all();
        $newfactors = $allParams['newfactors'];
        $updatedFactors = $allParams['updatedFactors'];

        if ($newfactors) {
          foreach ($newfactors as $i => $product) {
            $factor = new Factor();

            $factor->setTitleFi($newfactors[$i]['title_fi']);
            $factor->setTitleEn($newfactors[$i]['title_en']);
            $factor->setValue($newfactors[$i]['value']);
            $factor->setMeasure($newfactors[$i]['measure']);
            $factor->setSource($newfactors[$i]['source']);
            $factor->setReferenceId($newfactors[$i]['reference_id']);
            $factor->setLastUpdated(time());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($factor);
            $entityManager->flush();
          }
        }

        if ($updatedFactors) {
          foreach ($updatedFactors as $i => $product) {

            $factor = $this->getDoctrine()->getRepository(Factor::class)->find($updatedFactors[$i]['id']);
            $factor->setTitleFi($updatedFactors[$i]['title_fi']);
            $factor->setTitleEn($updatedFactors[$i]['title_en']);
            $factor->setValue($updatedFactors[$i]['value']);
            $factor->setMeasure($updatedFactors[$i]['measure']);
            $factor->setSource($updatedFactors[$i]['source']);
            $factor->setReferenceId($updatedFactors[$i]['reference_id']);
            $factor->setLastUpdated(time());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($factor);
            $entityManager->flush();
          }
        }

        return $this->redirect('/'.$locale.'/factor/');
      }

      } else {
        return $this->redirectToRoute('welcome');
      }
    } else {
      return $this->redirectToRoute('welcome');
    }

  }

    /**
     * @Route("/factor/downloadcsv", methods={"GET"})
     */
    public function getCsv(Security $security, Request $request) {

      if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
        if ($security->getUser()->getRole() == 'super-user') {

        $entityManager = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM factor";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        $tabsResults = $stmt->fetchAll();
        if (!$tabsResults) { return new JsonResponse(array('message' => $tabsResults)); }
        //array('id', 'title_fi','title_en', 'value', 'measure', 'source', 'reference_id', 'last_updated', $tabsResults)

        $container = array();
        $headings = array('id', 'title_fi','title_en', 'value', 'measure', 'source', 'reference_id', 'last_updated');
        array_push($container, $headings);
        foreach ($tabsResults as $i => $product) {
            if (trim($tabsResults[$i]['source']) == '') {
              $tabsResults[$i]['source'] = 'emptysource';
            }
            if (trim($tabsResults[$i]['reference_id']) == '') {
              $tabsResults[$i]['reference_id'] = 'emptyreference_id';
            }
            $tabsResults[$i]['last_updated'] = date('d.m.Y', $tabsResults[$i]['last_updated']);
            array_push($container, $tabsResults[$i]);
        }

        $list = $container;
        $fp = fopen('php://output', 'w');
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        //it's gonna output in a testing.csv file
        $response->headers->set('Content-Disposition', 'attachment; filename="testing.csv"');

        if ($response) {
          return $response;
        }


        } else {
          return $this->redirectToRoute('welcome');
        }
      } else {
        return $this->redirectToRoute('welcome');
      }

    }

    /**
     * @Route("/{locale}/factor", name="factor_list", methods={"GET"})
     */
    public function factor(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $userId = $security->getUser()->getId();

          $serviceFactor = new ServiceFactor($this->getDoctrine()->getManager(), Factor::class);
          $factors = $serviceFactor->getAllFactors();

          foreach ($factors as $i => $product) {
            $factors[$i]->lastUpdatedFi = date('d.m.Y', $factors[$i]->getLastUpdated());
            $factors[$i]->lastUpdatedEn = date('d/m/Y', $factors[$i]->getLastUpdated());
            $factors[$i]->valueFloated = (float)$factors[$i]->getValue();
          }

          return $this->render('factors/'.$locale.'.index.html.twig', array
          ('factors' => $factors));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route ("/{locale}/newfactor", methods={"GET"})
     */
    public function new(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $newFactor = new Factor();

        return $this->render('factors/'.$locale.'.new.html.twig',array(
            'factor' => $newFactor));
    }

    /**
     * @Route ("/{locale}/newfactor", methods={"POST"})
     */
    public function newPost(Security $security, Request $request, $locale) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $factor = new Factor();

      $factor->setTitleFi($request->get('titleFi'));
      $factor->setTitleEn($request->get('titleEn'));
      $factor->setValue($request->get('value'));
      $factor->setMeasure($request->get('measure'));
      $factor->setSource($request->get('source'));
      $factor->setReferenceId($request->get('referenceId'));
      $factor->setLastUpdated(time());

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($factor);
      $entityManager->flush();

      $factorId = $factor->getId();
      return $this->redirect('/'.$locale.'/factor/'.$factorId);
    }

    /**
     * @Route ("/{locale}/factor/{id}", name= "factor_show", methods={"GET"})
     */
    public function show(Security $security, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $factor = $this->getDoctrine()->getRepository(Factor::class)->find($id);

        if (!$factor) {
          return $this->redirect('/');
        }

        $factor->lastUpdatedFi = date('d.m.Y', $factor->getLastUpdated());
        $factor->lastUpdatedEn = date('d/m/Y', $factor->getLastUpdated());

        return $this->render('factors/'.$locale.'.show.html.twig', ['factor' => $factor]);

        //return $this->render('articles/show.html.twig', array('article' =>$article));

    }

    /**
     * @Route("/{locale}/factor/update/{id}", methods={"GET"})
     */
    public function update(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if ($security->getUser()->getRole() == '') {
          return new JsonResponse(array('message' => 'No permission!'));
        }

        $factor = $this->getDoctrine()->getRepository(Factor::class)->find($id);

        if (!$factor) {
          return $this->redirect('/');
        }

        $factor->lastUpdatedFi = date('d.m.Y', $factor->getLastUpdated());
        $factor->lastUpdatedEn = date('d/m/Y', $factor->getLastUpdated());

        return $this->render('factors/'.$locale.'.update.html.twig',array(
            'factor' => $factor ));
    }

    /**
     * @Route("/{locale}/factor/update/{id}", methods={"POST"})
     */
    public function updatePost(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if ($security->getUser()->getRole() == '') {
          return new JsonResponse(array('message' => 'No permission!'));
        }

        $em = $this->getDoctrine()->getManager();
        $factor = $em->getRepository(Factor::class)->find($id);

        if (!$factor) {
          throw $this->createNotFoundException(
            'No property found for id '.$id
          );
        }

        $factor->setTitleFi($request->get('titleFi'));
        $factor->setTitleEn($request->get('titleEn'));
        $factor->setValue($request->get('value'));
        $factor->setMeasure($request->get('measure'));
        $factor->setSource($request->get('source'));
        $factor->setReferenceId($request->get('referenceId'));
        $factor->setLastUpdated(time());

        $em->flush();

        $factorId = $factor->getId();
        return $this->redirect('/'.$locale.'/factor/'.$factorId);
        //return $this->redirect('/'.$locale.'/property/show/'.$id);
    }

    /**
     * @Route ("/{$locale}/factor/delete/{id}", methods={"GET"})
     */
    public function delete(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        //Todo
        return new JsonResponse(array('message' => 'Not done'));

    }
}
