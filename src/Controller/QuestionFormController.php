<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use App\Entity\Question;
use App\Entity\Factor;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
//services can have more functions
//service example: $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
//service example: $article = $articleService->getArticle($id);
use App\Service\ArticleService as ServiceArticle;
use App\Service\UserService as ServiceUser;
use App\Service\PropertyService as ServiceProperty;
use App\Service\CompanyService as CompanyService;
use App\Service\QuestionService as QuestionService;

class QuestionFormController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

    /**
     * @Route("/question/removeusertest", methods={"GET"})
     */
    public function test(Request $request) {

        $company = $this->getDoctrine()->getRepository(Company::class)->find(2);
        $user = $this->getDoctrine()->getRepository(User::class)->find(10);

        $company->removeUser($user);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($company);
        $entityManager->flush();

        $companyId = $company->getId();
        $locale = 'fi';
        return $this->redirect('/'.$locale.'/company/'.$companyId);

    }

    /**
     * @Route("/{locale}/questionform/updatetabs/{companyId}/{formName}/", methods={"GET"})
     */
    public function questionFormShowForm(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          $companyName = $company->getName();

          $entityManager = $this->getDoctrine()->getManager();
          $sql = "SELECT tab_name, form_name, content, company_id FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabsResults = $stmt->fetchAll();
          if (!$tabsResults) {
            return new JsonResponse(array('message' => 'No tab results!'));

          }

          $tabsArray = array();
          $addedInArray = array();
          foreach ($tabsResults as $i => $product) {
            $company = $this->getDoctrine()->getRepository(Company::class)->find($tabsResults[$i]['company_id']);
            $tabsResults[$i]['company_name'] = $company->getName();
            if (!in_array($tabsResults[$i]['tab_name'], $addedInArray)) {
              array_push($tabsArray, $tabsResults[$i]);
              array_push($addedInArray, $tabsResults[$i]['tab_name']);
            }
          }

          return $this->render('questionform/'.$locale.'.show-questionform.html.twig', array(
              'tabs' => $tabsArray, 'companyId' => $companyId, 'companyName' => $companyName, 'formName' => $formName ));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/updatetabs/{companyId}/{formName}/{tabName}", methods={"GET"})
     */
    public function questionFormUpdateTabs(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          //return new JsonResponse(array('message' => 'showtab!'));

          $entityManager = $this->getDoctrine()->getManager();
          $sql = "SELECT tab_name, form_name, content, company_id FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabsResults = $stmt->fetchAll();
          if (!$tabsResults) {
            return new JsonResponse(array('message' => 'No tab results!'));

          }

          $tabsArray = array();
          $addedInArray = array();
          foreach ($tabsResults as $i => $product) {
            $company = $this->getDoctrine()->getRepository(Company::class)->find($tabsResults[$i]['company_id']);
            $tabsResults[$i]['company_name'] = $company->getName();
            if ($tabsResults[$i]['tab_name'] !== $tabName && !in_array($tabsResults[$i]['tab_name'], $addedInArray)) {
              array_push($tabsArray, $tabsResults[$i]);
              array_push($addedInArray, $tabsResults[$i]['tab_name']);
            } else if ($tabsResults[$i]['tab_name'] == $tabName) {
              $selectedTab = $tabsResults[$i];
            }
          }

          //return new JsonResponse(array('message' => $tabsArray));

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND tab_name = '".$tabName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $questionsResults = $stmt->fetchAll();
          if (!$questionsResults) {
            return new JsonResponse(array('message' => 'No question results!'));
          }
/*
          foreach ($questionsResults as $i => $product) {
            $questionsResults[$i]['dateFormatted'] = date('Y-m-d', $questionsResults[$i]['target_time']);
          }
*/
          $factors = array();
          $sql = "SELECT id, title_fi, title_en FROM factor";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $factorsResults = $stmt->fetchAll();

          //return new JsonResponse(array('message' => $factorsResults));

          return $this->render('questionform/'.$locale.'.update-questionform.html.twig', array(
              'selectedTab' => $selectedTab, 'tabs' => $tabsArray, 'questions' => $questionsResults,
              'factors' => $factorsResults, 'companyId' => $companyId, 'formName' => $formName));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/updatetabs/{companyId}/{formName}/{tabName}", methods={"POST"})
     */
    public function questionFormUpdateTabsPost(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $entityManager = $this->getDoctrine()->getManager();
          $allParams = $request->request->all();
          $questions = $allParams['questions'];

          foreach ($questions as $i => $product) {

            $sql = "UPDATE question SET content='".$questions[$i]['content']."', tab_name='".$allParams['tabName']."', factor_id='".$questions[$i]['factor']."' WHERE id='".$questions[$i]['id']."';";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            //echo ($questions[$i]['id']);
          }

          //return new JsonResponse(array('message' => $allParams));
          return $this->redirect('/'.$locale.'/questionform/updatetabs/'.$companyId.'/'.$formName.'/'.$allParams['tabName']);

        } else {
          return $this->redirectToRoute('welcome');
        }

    }


    /**
     * @Route("/{locale}/questionform/", methods={"GET"})
     */
    public function questionForm(Security $security, Request $request, PaginatorInterface $paginator, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $entityManager = $this->getDoctrine()->getManager();
          $companyId = $request->get('companyId');
          if (!$companyId) {
            $companyId = 0;
          }

          $tabsArray = array();
          $formName = $request->get('formName');

          if ($formName) {

            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) {
              return new JsonResponse(array('message' => 'No tab results!'));
            }

            $tabsArray = array();
            foreach ($tabsResults as $i => $product) {
              array_push($tabsArray, $tabsResults[$i]['tab_name']);
            }
            $tabsArray = array_unique($tabsArray);

          } else {
            $formName = '';
          }
          //return new JsonResponse(array('message' => $tabsResults));

          $sql = "SELECT id, name FROM company";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $companiesResults = $stmt->fetchAll();

          $factors = array();
          $sql = "SELECT id, title_fi, title_en FROM factor";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $factorsResults = $stmt->fetchAll();

          $companyName = '';
          foreach ($companiesResults as $i => $product) {
            if ($companiesResults[$i]['id'] == $companyId) {
              $companyName = $companiesResults[$i]['name'];
            }
          }

          return $this->render('questionform/'.$locale.'.index.html.twig', array(
              'tabDone' => false, 'formName' => $formName,
              'companyId' => $companyId, 'companyName' => $companyName, 'tabs' => $tabsArray,
              'companies' => $companiesResults, 'factors' => $factorsResults));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/", methods={"POST"})
     */
    public function questionFormPost(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $entityManager = $this->getDoctrine()->getManager();
          $allParams = $request->request->all();

          $formName = $allParams['formName'];
          $tabName = $allParams['tabName'];
          $companyId = $allParams['companyId'];

          //return new JsonResponse(array('message' => $allParams));

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }

          $parents = $allParams['parents'];
          foreach ($parents as $i => $product) {
            $question = new Question();
            $factor = $this->getDoctrine()->getRepository(Factor::class)->find($parents[$i]['factor']);
            if (!$factor) { return new JsonResponse(array('message' => 'no factor!')); }

            $question->setContent($parents[$i]['content']);
            $question->setFormName($allParams['formName']);
            $question->setFactor($factor);
            $question->setCompany($company);
            $question->setTabName($tabName);
            //$question->setTargetTime(strtotime($parents[$i]['targetTime']));
            $question->setFormName($formName);

            $entityManager->persist($question);
            $entityManager->flush();

          }
          //return new JsonResponse(array('message' => $allParams));

          $entityManager = $this->getDoctrine()->getManager();
          $userId = $security->getUser()->getId();
          $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabsResults = $stmt->fetchAll();

          $tabsArray = array();
          foreach ($tabsResults as $i => $product) {
            array_push($tabsArray, $tabsResults[$i]['tab_name']);
          }
          $tabsArray = array_unique($tabsArray);

          //return new JsonResponse(array('message' => $tabsArray));
          $sql = "SELECT id, name FROM company";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $companiesResults = $stmt->fetchAll();

          $factors = array();
          $sql = "SELECT id, title_fi, title_en FROM factor";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $factorsResults = $stmt->fetchAll();

          return $this->redirect('/'.$locale.'/questionform/updatetabs/'.$companyId.'/'.$formName.'/');
/*
          return $this->render('questionform/'.$locale.'.index.html.twig', array(
              'tabDone' => true, 'formName' => $formName,
              'companyId' => $companyId, 'companyName' => '', 'tabs' => $tabsArray,
              'companies' => $companiesResults, 'factors' => $factorsResults));
*/
        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/newquestion/{companyId}/{formName}/{tabName}", methods={"GET"})
     */
    public function questionFormNewQuestion(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          //return new JsonResponse(array('message' => 'showtab!'));

          $entityManager = $this->getDoctrine()->getManager();
          $sql = "SELECT tab_name, form_name, content, company_id FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabsResults = $stmt->fetchAll();
          if (!$tabsResults) {
            return new JsonResponse(array('message' => 'No tab results!'));
          }

          $tabsArray = array();
          $addedInArray = array();
          foreach ($tabsResults as $i => $product) {
            $company = $this->getDoctrine()->getRepository(Company::class)->find($tabsResults[$i]['company_id']);
            $tabsResults[$i]['company_name'] = $company->getName();
            if ($tabsResults[$i]['tab_name'] !== $tabName && !in_array($tabsResults[$i]['tab_name'], $addedInArray)) {
              array_push($tabsArray, $tabsResults[$i]);
              array_push($addedInArray, $tabsResults[$i]['tab_name']);
            } else if ($tabsResults[$i]['tab_name'] == $tabName) {
              $selectedTab = $tabsResults[$i];
            }
          }

          //return new JsonResponse(array('message' => $tabsArray));

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND tab_name = '".$tabName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $questionsResults = $stmt->fetchAll();
          if (!$questionsResults) {
            return new JsonResponse(array('message' => 'No question results!'));
          }

          $factors = array();
          $sql = "SELECT id, title_fi, title_en FROM factor";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $factorsResults = $stmt->fetchAll();

          //return new JsonResponse(array('message' => $factorsResults));

          return $this->render('questionform/'.$locale.'.newquestion-questionform.html.twig', array(
              'selectedTab' => $selectedTab, 'tabs' => $tabsArray, 'questions' => $questionsResults,
              'factors' => $factorsResults, 'companyId' => $companyId, 'formName' => $formName));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/newquestion/{companyId}/{formName}/{tabName}", methods={"POST"})
     */
    public function questionFormNewQuestionPost(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $allParams = $request->request->all();
          $question = new Question();
          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          $factor = $this->getDoctrine()->getRepository(Factor::class)->find($allParams['factor']);
          if (!$company || !$factor) {
            return new JsonResponse(array('message' => 'company or factor not found!'));
          }
          $entityManager = $this->getDoctrine()->getManager();

          $question->setContent($allParams['content']);
          $question->setCompany($company);
          $question->setFactor($factor);
          $question->setTabName($tabName);
          $question->setFormName($formName);
          //$question->setTargetTime(strtotime($allParams['targetTime']));
          $entityManager->persist($question);
          $entityManager->flush();

          //return new JsonResponse(array('message' => $allParams));
          return $this->redirect('/'.$locale.'/questionform/updatetabs/'.$companyId.'/'.$formName.'/'.$tabName);

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route ("/{locale}/newquestion", methods={"GET"})
     */
    public function new(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $newQuestion = new Question();

        return $this->render('questions/'.$locale.'.new.html.twig', array(
            'question'=>$newQuestion ));
    }

    /**
     * @Route ("/{locale}/newquestion", methods={"POST"})
     */
    public function newPost(Security $security, Request $request, $locale) {

      $question = new Question();

      $entityManager = $this->getDoctrine()->getManager();

      $question->setContent($request->get('content'));
      $question->setCategory($request->get('category'));
      $question->setLanguage($request->get('language'));
      $entityManager->persist($question);
      $entityManager->flush();

      $questionId = $question->getId();
      return $this->redirect('/'.$locale.'/question/');

    }

    /**
     * @Route ("/{locale}/question/{id}", name= "question_show", methods={"GET"})
     */
    public function show(Security $security, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);

        if (!$question) {
          return $this->redirect('/');
        }

        return $this->render('questions/'.$locale.'.show.html.twig', ['question' => $question]);

    }

    /**
     * @Route("/{locale}/question/update/{id}", methods={"GET"})
     */
    public function update(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);

        if (!$question) {
          return $this->redirect('/');
        }

        return $this->render('questions/'.$locale.'.update.html.twig',array(
            'question' => $question ));
    }

    /**
     * @Route("/{locale}/question/update/{id}", methods={"POST"})
     */
    public function updatePost(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository(Question::class)->find($id);

        if (!$question) {
          throw $this->createNotFoundException(
            'No question found for id '.$id
          );
        }

        $question->setContent($request->get('content'));
        $question->setCategory($request->get('category'));
        $question->setLanguage($request->get('language'));

        $em->flush();

        return $this->redirect('/'.$locale.'/question/'.$id);
    }

    /**
     * @Route (" /{locale}/questionform/deletetab/{companyId}/{formName}/{tabName}", methods={"GET"})
     */
    public function deleteTab(Security $security, Request $request, $locale, $companyId, $formName, $tabName) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      //return new JsonResponse(array('message' => 'No can do!'));
      $userId = $security->getUser()->getId();
      $entityManager = $this->getDoctrine()->getManager();

      $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
      $hasPermission = $companyService->checkPermission($companyId, $userId);
      if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

      $sql = "SELECT * FROM user_companies WHERE company_id = '".$companyId."' AND user_id = '".$userId."'";
      $stmt = $entityManager->getConnection()->prepare($sql);
      $stmt->execute();
      $companyResults = $stmt->fetchAll();
      if (!$companyResults) { return new JsonResponse(array('message' => 'No permission!')); }

      $sql = "SELECT * FROM answer WHERE form_name = '".$formName."' AND tab_name = '".$tabName."' AND company_id = '".$companyId."'";
      $stmt = $entityManager->getConnection()->prepare($sql);
      $stmt->execute();
      $answerResults = $stmt->fetchAll();
      if (count($answerResults) > 0) {
        return new JsonResponse(array('message' => 'Form has questions with answers, can not delete!'));
      }

      $sql = "DELETE FROM question WHERE company_id = '".$companyId."' AND form_name = '".$formName."' AND tab_name = '".$tabName."'";
      $stmt = $entityManager->getConnection()->prepare($sql);
      $stmt->execute();

      return $this->redirect('/'.$locale.'/company/'.$companyId);
    }

    /**
     * @Route (" /{locale}/questionform/deletequestion/{companyId}/{formName}/{tabName}/{questionId}", methods={"GET"})
     */
    public function deleteQuestion(Security $security, Request $request, $locale, $companyId, $formName, $tabName, $questionId) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $userId = $security->getUser()->getId();
      $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
      $hasPermission = $companyService->checkPermission($companyId, $userId);
      if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

      $questionService = new QuestionService($this->getDoctrine()->getManager(), Question::class);
      $questionService->deleteQuestion($questionId);

      return $this->redirect('/'.$locale.'/questionform/updatetabs/'.$companyId.'/'.$formName.'/');

    }
}
