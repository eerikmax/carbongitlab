<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use App\Entity\Question;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
//services can have more functions
//service example: $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
//service example: $article = $articleService->getArticle($id);
use App\Service\ArticleService as ServiceArticle;
use App\Service\UserService as ServiceUser;
use App\Service\PropertyService as ServiceProperty;
use App\Service\CompanyService as CompanyService;
use App\Service\QuestionService as QuestionService;

class QuestionController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

    /**
     * @Route("/question/removeusertest", methods={"GET"})
     */
    public function test(Request $request) {

        $company = $this->getDoctrine()->getRepository(Company::class)->find(2);
        $user = $this->getDoctrine()->getRepository(User::class)->find(10);

        $company->removeUser($user);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($company);
        $entityManager->flush();

        $companyId = $company->getId();
        $locale = 'fi';
        return $this->redirect('/'.$locale.'/company/'.$companyId);

    }

    /**
     * @Route("/{locale}/question", name="question_list", methods={"GET"})
     */
    public function question(Security $security, Request $request, PaginatorInterface $paginator, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
          $filteredBy = $request->get('filteredBy');
          $language = $request->get('language');
          $pageCount = $request->get('page');

          $questionService = new QuestionService($this->getDoctrine()->getManager(), Question::class);

          if (!$filteredBy) { $filteredBy = 'all'; }
          if (!$language) { $language = 'all'; }
          if (!$pageCount) { $pageCount = 1; }

          if ($filteredBy !== 'all') {
            $questions = $questionService->getAllQuestionsByCategory($filteredBy);
          } else {
            $questions = $questionService->getAllQuestionsInArray();
          }

          if ($language == 'english' || $language == 'finnish') {
            $questions = $questionService->filterQuestionsByLanguage($questions, $language);
            //echo print_r($questions);
          }

          $allQuestionsLength = count($questions);

          // Paginate the results of the query
          $questions = $paginator->paginate(
              // Doctrine Query, not results
              $questions,
              // Define the page parameter
              $request->query->getInt('page', 1),
              // Items per page
              5
          );

          $currentPageQuestionsFirst = ((5 * ($pageCount - 1)) + count($questions)) - count($questions) + 1;
          $currentPageQuestionsLast = (5 * ($pageCount - 1)) + count($questions);

          return $this->render('questions/'.$locale.'.index.html.twig', array
          ('questions' => $questions,
          'filteredBy' => $filteredBy,
          'language' => $language,
          'allQuestionsLength' => $allQuestionsLength,
          'pageCount' => $pageCount,
          'currentPageQuestionsFirst' => $currentPageQuestionsFirst,
          'currentPageQuestionsLast' => $currentPageQuestionsLast
        ));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route ("/{locale}/newquestion", methods={"GET"})
     */
    public function new(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $newQuestion = new Question();

        return $this->render('questions/'.$locale.'.new.html.twig', array(
            'question'=>$newQuestion ));
    }

    /**
     * @Route ("/{locale}/newquestion", methods={"POST"})
     */
    public function newPost(Security $security, Request $request, $locale) {

      $question = new Question();

      $entityManager = $this->getDoctrine()->getManager();

      $question->setContent($request->get('content'));
      $question->setCategory($request->get('category'));
      $question->setLanguage($request->get('language'));
      $entityManager->persist($question);
      $entityManager->flush();

      $questionId = $question->getId();
      return $this->redirect('/'.$locale.'/question/');

    }

    /**
     * @Route ("/{locale}/question/{id}", name= "question_show", methods={"GET"})
     */
    public function show(Security $security, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);

        if (!$question) {
          return $this->redirect('/');
        }

        return $this->render('questions/'.$locale.'.show.html.twig', ['question' => $question]);

    }

    /**
     * @Route("/{locale}/question/update/{id}", methods={"GET"})
     */
    public function update(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);

        if (!$question) {
          return $this->redirect('/');
        }

        return $this->render('questions/'.$locale.'.update.html.twig',array(
            'question' => $question ));
    }

    /**
     * @Route("/{locale}/question/update/{id}", methods={"POST"})
     */
    public function updatePost(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository(Question::class)->find($id);

        if (!$question) {
          throw $this->createNotFoundException(
            'No question found for id '.$id
          );
        }

        $question->setContent($request->get('content'));
        $question->setCategory($request->get('category'));
        $question->setLanguage($request->get('language'));

        $em->flush();

        return $this->redirect('/'.$locale.'/question/'.$id);
    }

    /**
     * @Route ("/{locale}/question/delete/{id}", methods={"GET"})
     */
    public function delete(Security $security, Request $request, $locale, $id) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }
/*
      $em = $this->getDoctrine()->getManager();
      $article = $em->getRepository(Company::class)->find($id);

      //check if own company???

      $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
      $companyService->deleteCompany($id);
*/
      return $this->redirect('/'.$locale.'/company/');
    }
}
